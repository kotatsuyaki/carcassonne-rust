use std::cell::RefCell;

use ccs::{Action, Board, Player, TileKind};
use hashbrown::HashMap;
use indextree::{Arena, NodeId};
use once_cell::unsync::OnceCell;
use ord_subset::OrdSubsetIterExt;
use rand::prelude::*;
use tracing::{debug, info, warn};

#[derive(Debug)]
pub struct Mcts {
    arena: Arena<Data>,
    root: Option<NodeId>,
    expand_count: RefCell<usize>,
}

#[derive(Debug)]
enum Data {
    Determ {
        state: OnceCell<Box<Board>>,
        last_draw: TileKind,
        visits: usize,
        weight: usize,
    },
    Undeterm {
        state: OnceCell<Box<Board>>,
        last_action: Action,
        value: f32,
        visits: usize,
    },
}

impl Mcts {
    pub fn new() -> Self {
        Mcts {
            arena: Arena::new(),
            root: None,
            expand_count: RefCell::new(0),
        }
    }

    pub fn stats(&self) {
        info!("expand count = {}", self.expand_count.borrow());
    }

    pub fn action_prob(&self) -> HashMap<Action, f32> {
        let mut prob = HashMap::new();
        let mut total_visits = 0;

        // for each action, initialize to visit count
        let root = self.root.unwrap();
        for child in root.children(&self.arena) {
            let visits = *self.arena[child].get().visits();
            let action = *self.arena[child].get().last_action();
            prob.insert(action, visits as f32);
            total_visits += visits;
        }

        if total_visits != 0 {
            // divide by total visits
            prob.values_mut()
                .for_each(|value| *value /= total_visits as f32);
        } else {
            // fallback to uniform 1/n
            warn!("Total visits is 0");

            let len = prob.len();
            prob.values_mut()
                .for_each(|value| *value = 1.0 / len as f32);
        }

        prob
    }

    /// Resets the search tree of this Mcts.
    pub fn cleanup_arena(&mut self) {
        self.root = None;
        self.arena = Arena::new();
    }

    pub fn mcts_loop(&mut self, from_state: Board, iters: usize) -> Action {
        assert_eq!(from_state.is_ended(), false);

        // setup root
        // search begins from deterministic node
        let root = self.arena.new_node(Data::Determ {
            weight: 0,
            state: OnceCell::new(),
            last_draw: from_state.deck_iter().last().unwrap(),
            visits: 0,
        });
        self.arena[root].get().set_state(from_state.clone());
        self.root = Some(root);

        for _ in 0..iters {
            let mut current = root;

            let mut is_ended = false;
            while self.has_children(current) {
                // From D, select U
                current = match self.select(current) {
                    Some(node) => node,
                    None => {
                        debug!("Dead end during selection");
                        continue;
                    }
                };
                assert!(self.is_type_u(current));
                self.expand_draws(current);

                // From U, try to sample D
                if let Some(sampled) = self.sample_draw(current) {
                    current = sampled;
                    assert!(self.is_type_d(current));
                } else {
                    // Leave `current` as type-U and don't expand
                    is_ended = true;
                    break;
                }
            }

            let winner = if is_ended == false {
                assert!(self.is_type_d(current));

                self.expand_actions(current);
                current = match self.sample_action(current) {
                    Some(node) => node,
                    None => {
                        debug!("Dead end during expansion");
                        continue;
                    }
                };
                assert!(self.is_type_u(current));

                self.expand_draws(current);
                if self.state(current).is_ended() == false {
                    current = self.sample_draw(current).unwrap();

                    assert!(self.is_type_d(current));

                    // simulate game
                    let winner = self.simulate(current);
                    winner
                } else {
                    let winner = self.state(current).winner();
                    winner
                }
            } else {
                assert!(self.is_type_u(current));

                // get the winner
                let winner = self.state(current).winner();
                winner
            };

            // propagate result
            loop {
                let node = self.arena[current].get_mut();
                let state = node.state();
                let score = match winner {
                    Some(winner) if winner == state.turn() => 0.0,
                    // Some(winner) if winner == node.state().turn() => 0.0,
                    Some(_winner) => 1.0,
                    None => 0.5,
                };

                node.increase_value(score);
                *node.visits_mut() += 1;

                if let Some(parent) = self.arena[current].parent() {
                    current = parent;
                } else {
                    break;
                }
            }
        }

        let max = root
            .children(&self.arena)
            .max_by_key(|&child| self.arena[child].get().visits());
        if let Some(max) = max {
            *self.arena[max].get().last_action()
        } else {
            warn!("No max child, falling back to random");
            *from_state
                .legal_actions()
                .choose(&mut thread_rng())
                .unwrap()
        }
    }

    fn has_children(&self, current: NodeId) -> bool {
        current.children(&self.arena).next().is_some()
    }

    /// Selects a child from a type-D node and return that child node.
    ///
    /// Random child is returned if all UCT scores are not within the ordered float subset.
    /// `None` is returned if the type-D node has no legal action at all.
    fn select(&self, current: NodeId) -> Option<NodeId> {
        assert!(self.is_type_d(current));

        let parent_visits = *self.arena[current].get().visits() as f32;

        let selected = current
            .children(&self.arena)
            .ord_subset_max_by_key(|&child| {
                let value = *self.arena[child].get().value();
                let visits = *self.arena[child].get().visits() as f32 + 1.0;

                (value / visits) + f32::sqrt(2.0) * f32::sqrt(f32::ln(parent_visits) / visits)
            });
        let random_child = || {
            warn!("Selected node is None");
            current.children(&self.arena).choose(&mut thread_rng())
        };
        selected.or_else(random_child)
    }

    /// Samples a draw from a type-U node and return that child node.
    ///
    /// `None` is returned if the type-U node is ended.
    fn sample_draw(&self, current: NodeId) -> Option<NodeId> {
        assert!(self.is_type_u(current));

        if self.state(current).is_ended() {
            return None;
        }

        let candidates = current.children(&self.arena).collect::<Vec<_>>();
        let sampled = candidates
            .choose_weighted(&mut thread_rng(), |&child| self.arena[child].get().weight());

        if let Err(_) = sampled {
            warn!("Sampled node is None");
        }
        sampled.ok().cloned()
    }

    /// Samples an action a type-D node and return that child node.
    ///
    /// `None` is returned if the type-D node has no legal action at all.
    fn sample_action(&self, current: NodeId) -> Option<NodeId> {
        assert!(self.is_type_d(current));

        current.children(&self.arena).choose(&mut thread_rng())
    }

    /// Expands all the children type-U nodes from a type-D node by enumerating legal actions.
    ///
    /// Does nothing if there are no legal actions.
    fn expand_actions(&mut self, current: NodeId) {
        assert!(self.is_type_d(current));

        let legal_actions = self.state(current).legal_actions();

        for action in legal_actions {
            // append child
            let child = self.arena.new_node(Data::Undeterm {
                state: OnceCell::new(),
                last_action: action,
                value: 0.0,
                visits: 0,
            });
            current.append(child, &mut self.arena);
        }
    }

    /// Expands all the children type-D nodeds from a type-U node by enumerating possible draws.
    ///
    /// Does nothing if the node has already been expanded.
    fn expand_draws(&mut self, current: NodeId) {
        assert!(self.is_type_u(current));

        if self.has_children(current) {
            return;
        }

        let state = self.state(current);

        for (draw, weight) in state.legal_draws() {
            // append child
            let child = self.arena.new_node(Data::Determ {
                weight,
                state: OnceCell::new(),
                last_draw: draw,
                visits: 0,
            });
            current.append(child, &mut self.arena);
        }
    }

    fn simulate(&self, from_node: NodeId) -> Option<Player> {
        const DRAW_RETRY_THRESHOLD: usize = 10;
        assert!(self.is_type_d(from_node));

        let mut state = self.state(from_node).clone();

        while state.is_ended() == false {
            let mut draw_times = 0;
            let legal_actions = loop {
                state.draw();
                let legal_actions = state.legal_actions();
                if legal_actions.is_empty() == false {
                    break legal_actions;
                }

                draw_times += 1;
                if draw_times >= DRAW_RETRY_THRESHOLD {
                    warn!(
                        "Failed to find draw with legal positions within {} trials",
                        DRAW_RETRY_THRESHOLD
                    );
                    return None;
                }
            };
            let action = *legal_actions.choose(&mut thread_rng()).unwrap();

            state.do_action(action);
        }

        state.winner()
    }

    fn is_type_u(&self, node: NodeId) -> bool {
        self.arena[node].get().is_type_u()
    }

    fn is_type_d(&self, node: NodeId) -> bool {
        self.arena[node].get().is_type_d()
    }

    fn state(&self, node: NodeId) -> &Board {
        let state = match self.arena[node].get() {
            Data::Determ { ref state, .. } => state.get().map(|boxed| boxed.as_ref()),
            Data::Undeterm { ref state, .. } => state.get().map(|boxed| boxed.as_ref()),
        };
        if let Some(state) = state {
            state
        } else {
            self.expand_count.replace_with(|&mut old| old + 1);

            // Clone parent's state and apply action / draw
            let parent = self.arena[node].parent().unwrap();
            let mut state = self.state(parent).clone();

            match self.arena[node].get() {
                Data::Determ { last_draw, .. } => state.do_draw(*last_draw),
                Data::Undeterm { last_action, .. } => state.do_action(*last_action),
            }

            self.arena[node].get().set_state(state);
            self.state(node)
        }
    }
}

impl Data {
    /// Force (unwrap) getting the state.
    ///
    /// **Panics** if the state is not yet set.
    fn state(&self) -> &Board {
        match self {
            Self::Determ { ref state, .. } => state.get().map(|boxed| boxed.as_ref()).unwrap(),
            Self::Undeterm { ref state, .. } => state.get().map(|boxed| boxed.as_ref()).unwrap(),
        }
    }

    /// Sets the internal state to `board`.
    ///
    /// **Panics** if the state has already been set.
    fn set_state(&self, board: Board) {
        let state = match self {
            Self::Determ { ref state, .. } => state,
            Self::Undeterm { ref state, .. } => state,
        };
        state.set(Box::new(board)).unwrap();
    }

    fn is_type_u(&self) -> bool {
        matches!(self, Self::Undeterm { .. })
    }

    fn is_type_d(&self) -> bool {
        matches!(self, Self::Determ { .. })
    }

    /// Increases the node value by `diff`.
    ///
    /// Does nothing if the node is type-D, because value is meaningful only for type-U nodes.
    fn increase_value(&mut self, diff: f32) {
        match self {
            Data::Determ { .. } => {}
            Data::Undeterm { ref mut value, .. } => *value += diff,
        }
    }

    fn visits_mut(&mut self) -> &mut usize {
        match self {
            Data::Determ { ref mut visits, .. } => visits,
            Data::Undeterm { ref mut visits, .. } => visits,
        }
    }

    /// Returns the value of the type-U node.
    ///
    /// **Panics** if the node is type-D.
    fn value(&self) -> &f32 {
        match self {
            Data::Determ { .. } => panic!("Attempted to get value of type-D node"),
            Data::Undeterm { ref value, .. } => value,
        }
    }

    fn visits(&self) -> &usize {
        match self {
            Data::Determ { ref visits, .. } => visits,
            Data::Undeterm { ref visits, .. } => visits,
        }
    }

    /// Get the last action of the type-U node.
    ///
    /// **Panics** if the node is type-D.
    fn last_action(&self) -> &Action {
        match self {
            Data::Undeterm {
                ref last_action, ..
            } => last_action,
            Data::Determ { .. } => {
                panic!("Attempted to get last action of type-D node")
            }
        }
    }

    fn weight(&self) -> usize {
        match self {
            Data::Determ { weight: count, .. } => *count,
            Data::Undeterm { .. } => panic!("Attempted to get weight of type-U node"),
        }
    }
}
