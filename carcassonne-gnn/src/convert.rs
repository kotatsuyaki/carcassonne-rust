use ccs::{
    Action, Board, Coord, Feature, FeatureId, FloodfillConfig, MeepleAction, MeepleCoord, Player,
    Segment, Tile,
};
use hashbrown::{HashMap, HashSet};
use ndarray::prelude::*;
use num_traits::cast::FromPrimitive;
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;

#[derive(Debug, Serialize, Deserialize)]
pub struct Graph {
    pub edges: ArrayD<i64>,
    pub nodes: ArrayD<f32>,
}

impl Graph {
    pub fn num_nodes(&self) -> u64 {
        self.nodes.shape()[0] as u64
    }
}

#[derive(Debug)]
enum Node {
    Tile {
        coord: Coord,
        real: bool,
    },
    Area {
        coord: Coord,
        feature: Feature,
        is_closed: bool,
    },
    Feature {
        coord: Coord,
        feature: Feature,
        meeple: Option<Player>,
        real: bool,
    },
}

impl Node {
    fn to_array(
        &self,
        me: Player,
        my_score: u8,
        their_score: u8,
        arr: &mut ArrayD<f32>,
        row: usize,
    ) {
        const I: usize = 0;
        const J: usize = 1;
        const ISTILE: usize = 2;
        const ISAREA: usize = 3;
        const ISFEATURE: usize = 4;
        const REAL: usize = 5;
        const HASCITY: usize = 6;
        const HASROAD: usize = 7;
        const HASFIELD: usize = 8;
        const HASCLOISTER: usize = 9;
        const MEEPLESIDE: usize = 10;
        const MYSCORE: usize = 11;
        const THEIRSCORE: usize = 12;
        const CLOSED: usize = 13;

        match self {
            &Node::Tile { coord, real } => {
                arr[[row, I]] = coord.i as f32;
                arr[[row, J]] = coord.j as f32;
                arr[[row, ISTILE]] = 1.0;
                arr[[row, REAL]] = if real { 1.0 } else { 0.0 };
                arr[[row, MYSCORE]] = my_score as f32;
                arr[[row, THEIRSCORE]] = their_score as f32;
            }
            &Self::Area {
                coord,
                feature,
                is_closed,
            } => {
                arr[[row, I]] = coord.i as f32;
                arr[[row, J]] = coord.j as f32;
                arr[[row, ISAREA]] = 1.0;
                arr[[row, REAL]] = 1.0;
                arr[[row, MYSCORE]] = my_score as f32;
                arr[[row, THEIRSCORE]] = their_score as f32;
                arr[[row, CLOSED]] = if is_closed { 1.0 } else { 0.0 };

                let feat_index = match feature {
                    Feature::City => HASCITY,
                    Feature::Field => HASFIELD,
                    Feature::Road => HASROAD,
                    Feature::Cloister => HASCLOISTER,
                };
                arr[[row, feat_index]] = 1.0;
            }
            &Node::Feature {
                coord,
                feature,
                meeple,
                real,
            } => {
                arr[[row, I]] = coord.i as f32;
                arr[[row, J]] = coord.j as f32;
                arr[[row, ISFEATURE]] = 1.0;
                arr[[row, REAL]] = if real { 1.0 } else { 0.0 };
                arr[[row, MYSCORE]] = my_score as f32;
                arr[[row, THEIRSCORE]] = their_score as f32;

                let feat_index = match feature {
                    Feature::City => HASCITY,
                    Feature::Field => HASFIELD,
                    Feature::Road => HASROAD,
                    Feature::Cloister => HASCLOISTER,
                };
                arr[[row, feat_index]] = 1.0;
                if let Some(meeple) = meeple {
                    arr[[row, MEEPLESIDE]] = if meeple == me { 1.0 } else { -1.0 };
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct GraphConvertOutput {
    pub graph: Graph,
    pub candidate_node_indexes: HashMap<Action, usize>,
    pub nodes_len: usize,
}

pub fn graph_from_board(board: Board) -> GraphConvertOutput {
    let mut tile_node_indexes = HashMap::new();
    let mut feature_node_indexes = HashMap::new();
    let mut candidate_node_indexes = HashMap::new();
    let mut fake_feature_node_indexes = HashMap::new();

    let mut nodes = vec![];
    let mut edges = HashSet::new();

    // temporary map to lookup meeple by Coord
    let meeples: HashMap<_, _> = board.meeples_iter().collect();

    let mut tile_entries: Vec<(Coord, Tile)> = board.tiles_iter().collect();
    tile_entries.sort_by_key(|entry| entry.0);

    // tile nodes
    for &(coord, _tile) in tile_entries.iter() {
        tile_node_indexes.insert(coord, nodes.len());
        nodes.push(Node::Tile { coord, real: true });
    }

    // tile-tile edges
    for &(coord, _tile) in tile_entries.iter() {
        let own_index = tile_node_indexes[&coord];
        for (_side, neighbor) in coord.neighbors() {
            if let Some(&other_index) = tile_node_indexes.get(&neighbor) {
                edges.insert((own_index, other_index));
            }
        }
    }

    // feature nodes & feature-tile edges
    for &(coord, tile) in tile_entries.iter() {
        let tile_index = tile_node_indexes[&coord];

        for (feature_id, feature) in tile.features() {
            let feature_index = nodes.len();
            // connect with tile node
            edges.insert((tile_index, feature_index));
            // insert node
            feature_node_indexes.insert((coord, feature_id), nodes.len());

            let meeple = meeples
                .get(&MeepleCoord::Edge(coord, feature_id as u8))
                .cloned();

            nodes.push(Node::Feature {
                feature,
                coord,
                real: true,
                meeple,
            });
        }

        if tile.has_cloister() {
            let feature_index = nodes.len();
            let meeple = meeples.get(&MeepleCoord::Cloister(coord)).cloned();

            edges.insert((tile_index, feature_index));
            nodes.push(Node::Feature {
                feature: Feature::Cloister,
                coord,
                meeple,
                real: true,
            })
        }
    }

    // feature-feature edges
    for &(coord, tile) in tile_entries.iter() {
        for segment in Segment::iter() {
            let own_feature_id = tile.feature_id_of(segment);
            let own_index = feature_node_indexes[&(coord, own_feature_id)];

            // same-color edges
            let neighbor_coord = coord.neighbor_on(segment.side());
            if let Some(neighbor) = board.tile_at(neighbor_coord) {
                let other_feature_id = neighbor.feature_id_of(segment.opposite());
                let other_index = feature_node_indexes[&(neighbor_coord, other_feature_id)];

                edges.insert((own_index, other_index));
            }

            // adjacent field-city edges
            for city_feature_id in tile.adjacent_city_feature_ids(own_feature_id) {
                let city_index = feature_node_indexes[&(coord, city_feature_id)];
                edges.insert((own_index, city_index));
            }
        }
    }

    // area nodes & area-feature edges

    // clone for floodfill
    let mut board = board.clone();
    let mut visited: HashSet<(Coord, FeatureId)> = HashSet::new();
    for &(coord, tile) in tile_entries.iter() {
        for (feature_id, feature) in tile.features() {
            if visited.contains(&(coord, feature_id)) {
                continue;
            }

            let output = board.floodfill(FloodfillConfig {
                from_coord: coord,
                from_feature_id: feature_id,
                should_set_occupied: false,
            });

            let area_index = nodes.len();
            nodes.push(Node::Area {
                is_closed: output.is_closed,
                coord,
                feature,
            });
            output.visited.iter().for_each(|&(coord, feature_id)| {
                let feature_node_index = feature_node_indexes[&(coord, feature_id)];
                edges.insert((area_index, feature_node_index));
            });
            visited.extend(&output.visited);
        }
    }

    // candidate nodes & candidate-tile edges
    let mut legal_actions = board.legal_actions();
    legal_actions.sort_unstable();

    for action in legal_actions.iter() {
        let &Action { coord, .. } = action;

        let own_index = nodes.len();
        candidate_node_indexes.insert(*action, own_index);

        // candidate-tile edges
        for (_side, neighbor) in coord.neighbors() {
            if let Some(&other_index) = tile_node_indexes.get(&neighbor) {
                edges.insert((own_index, other_index));
            }
        }

        nodes.push(Node::Tile { coord, real: false });
    }

    // fakefeature nodes & fakefeature-candidate edges
    for action in legal_actions.iter() {
        let kind = board.deck_iter().last().unwrap();

        let &Action {
            coord,
            rotation,
            meeple_action,
        } = action;

        let tile = Tile { kind, rotation };
        let candidate_index = candidate_node_indexes[&action];

        for (feature_id, feature) in tile.features() {
            let fake_feature_index = nodes.len();

            let meeple = match meeple_action {
                MeepleAction::Edge(value) if FeatureId::from_u8(value).unwrap() == feature_id => {
                    Some(board.turn())
                }
                _ => None,
            };

            fake_feature_node_indexes.insert((*action, feature_id), nodes.len());
            edges.insert((candidate_index, fake_feature_index));
            nodes.push(Node::Feature {
                coord,
                feature,
                meeple,
                real: false,
            })
        }

        if tile.has_cloister() {
            let fake_feature_index = nodes.len();
            let meeple = match meeple_action {
                MeepleAction::Cloister => Some(board.turn()),
                _ => None,
            };

            edges.insert((candidate_index, fake_feature_index));
            nodes.push(Node::Feature {
                coord,
                feature: Feature::Cloister,
                meeple,
                real: false,
            });
        }
    }

    // fakefeature-feature edges
    for action in legal_actions.iter() {
        let kind = board.deck_iter().last().unwrap();

        let &Action {
            coord, rotation, ..
        } = action;

        let tile = Tile { kind, rotation };

        for segment in Segment::iter() {
            let own_feature_id = tile.feature_id_of(segment);
            let own_index = fake_feature_node_indexes[&(*action, own_feature_id)];

            let neighbor_coord = coord.neighbor_on(segment.side());
            if let Some(neighbor) = board.tile_at(neighbor_coord) {
                let other_feature_id = neighbor.feature_id_of(segment.opposite());
                let other_index = feature_node_indexes[&(neighbor_coord, other_feature_id)];

                edges.insert((own_index, other_index));
            }
        }
    }

    // convert set of edges to array
    let mut from_row = ArrayD::zeros(IxDyn(&[edges.len()]));
    let mut to_row = ArrayD::zeros(IxDyn(&[edges.len()]));
    for (i, &(from, to)) in edges.iter().enumerate() {
        from_row[i] = from as i64;
        to_row[i] = to as i64;
    }
    let edges = ndarray::stack(Axis(0), &[from_row.view(), to_row.view()]).unwrap();

    // convert nodes to array
    let me = board.turn();
    let nodes_len = nodes.len();
    let mut nodes_arr = ArrayD::zeros(IxDyn(&[nodes_len, 14]));
    for (row, node) in nodes.iter().enumerate() {
        node.to_array(
            me,
            board.scores()[me as u8 as usize],
            board.scores()[me.opposite() as u8 as usize],
            &mut nodes_arr,
            row,
        );
    }

    GraphConvertOutput {
        candidate_node_indexes,
        nodes_len,
        graph: Graph {
            edges,
            nodes: nodes_arr,
        },
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use rand::prelude::*;

    #[test]
    fn test_state_conversion_runs() {
        let mut board = Board::new();

        while board.is_ended() == false {
            let mut draw_times = 0;
            let legal_actions = loop {
                board.draw();
                let legal_actions = board.legal_actions();
                if legal_actions.is_empty() == false {
                    break legal_actions;
                }

                draw_times += 1;
                if draw_times >= 1000 {
                    panic!("All remaining tiles have no legal positions");
                }
            };
            let action = *legal_actions.choose(&mut thread_rng()).unwrap();

            let GraphConvertOutput { .. } = graph_from_board(board.clone());
            board.do_action(action);
        }
    }
}
