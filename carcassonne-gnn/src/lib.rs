use std::path::Path;
use std::time::Duration;
use std::time::Instant;

use anyhow::Result;
use ccs::{Action, Board};
use convert::Graph;
use hashbrown::HashMap;
use itertools::{izip, Itertools};
use maplit::hashmap;
use ndarray::prelude::*;
use numpy::PyArray;
use pyo3::types::IntoPyDict;
use pyo3::types::PyDict;
use pyo3::types::PyList;
use pyo3::types::PyTuple;
use serde::{Deserialize, Serialize};
use tokio::task;

use numpy::ToPyArray;
use pyo3::prelude::*;

use mailbox::{Client, ClientId, Host, Responder};
use tracing::{error, info};

use crate::convert::graph_from_board;
use crate::convert::GraphConvertOutput;

pub mod convert;
pub mod mailbox;
pub mod mcts;

#[derive(Debug)]
pub struct EvalOutput {
    value: f32,
    policy_map: HashMap<Action, f32>,
}

#[derive(Debug)]
pub struct EvalHost {
    host: Host<Board, EvalOutput>,
    model: PyObject,
}

#[derive(Debug)]
pub struct EvalClient {
    client: Client<Board, EvalOutput>,
}

impl EvalHost {
    /// Create a new [EvalHost].
    ///
    /// The model will be loaded from `./saves/model.pt` if it exists.
    /// Otherwise, the model will be created.
    pub async fn new(
        num_clients: usize,
        max_batch_size: usize,
        timeout: Duration,
    ) -> Result<(Self, Vec<EvalClient>)> {
        let (host, clients) = Host::create(num_clients, max_batch_size, timeout);

        let clients = clients
            .into_iter()
            .map(|client| EvalClient { client })
            .collect_vec();

        let model = Python::with_gil(|py| -> PyResult<PyObject> {
            let syspath: &PyList = py.import("sys")?.getattr("path")?.try_into()?;
            syspath.insert(0, "./")?;

            let model = PyModule::import(py, "model")?.getattr("Model")?;

            let model = model.call0()?;
            let model = model.getattr("to")?.call(("cuda",), None)?;

            info!("created model");
            Ok(model.to_object(py))
        })?;

        if Path::new("./saves/model.pt").is_file() {
            Python::with_gil(|py| -> PyResult<()> {
                let torch_load = PyModule::import(py, "torch")?.getattr("load")?;

                let checkpoint = torch_load.call1(("./saves/model.pt",))?;
                let model_state_dict = checkpoint.get_item("model_state_dict")?;
                let gen: u64 = checkpoint.get_item("gen")?.extract()?;

                info!("Loading model gen {}", gen);

                model.call_method1(py, "load_state_dict", (model_state_dict,))?;
                Ok(())
            })?;
        }

        Ok((Self { host, model }, clients))
    }

    pub async fn start_batch_loop(&mut self) -> Result<()> {
        const REPORT_INTERNVAL: u64 = 10;
        let mut last_reported = Instant::now();
        let mut num_inf_calls = 0;
        let mut num_requests = 0;

        while let Some((requesters, requests)) = self.host.get_requests().await {
            let responder = self.host.responder();

            num_inf_calls += 1;
            num_requests += requests.len();
            let now = Instant::now();
            if now - last_reported >= Duration::from_secs(REPORT_INTERNVAL) {
                info!(
                    "{:4} inferences/sec ; {:4} graphs/sec",
                    num_inf_calls as f32 / REPORT_INTERNVAL as f32,
                    num_requests as f32 / REPORT_INTERNVAL as f32,
                );
                num_inf_calls = 0;
                num_requests = 0;
                last_reported = now;
            }
            let model = self.model.clone();

            task::spawn(async move {
                if let Err(e) = Self::handle(model, requesters, requests, responder).await {
                    error!("handle() returned error: {:#?}", e);
                }
            });
        }

        Ok(())
    }

    async fn handle(
        model: PyObject,
        requesters: Vec<ClientId>,
        requests: Vec<Board>,
        responder: Responder<EvalOutput>,
    ) -> Result<(), anyhow::Error> {
        let responses = task::spawn_blocking(move || -> PyResult<Vec<EvalOutput>> {
            // inference
            let mut graphs = vec![];
            let mut candidate_node_indexeses = vec![];
            let mut nodes_lens = vec![];

            // Collect the batch
            for board in requests.into_iter() {
                let GraphConvertOutput {
                    graph,
                    candidate_node_indexes,
                    nodes_len,
                } = graph_from_board(board);

                graphs.push(graph);
                candidate_node_indexeses.push(candidate_node_indexes);
                nodes_lens.push(nodes_len);
            }

            let (v, p) = match Python::with_gil(|py| -> PyResult<(ArrayD<f32>, ArrayD<f32>)> {
                // imports
                let new_data = PyModule::import(py, "torch_geometric.data")?.getattr("Data")?;
                let new_batch = PyModule::import(py, "torch_geometric.data")?
                    .getattr("Batch")?
                    .getattr("from_data_list")?;
                let to_undirected =
                    PyModule::import(py, "torch_geometric.utils")?.getattr("to_undirected")?;
                let to_torch = PyModule::import(py, "torch")?.getattr("from_numpy")?;
                let no_grad = PyModule::import(py, "torch")?.getattr("no_grad")?.call0()?;

                // create data list and batch
                let mut data_list = vec![];
                for (graph, node_len) in graphs.into_iter().zip(nodes_lens.iter()) {
                    let nodes = graph.nodes.to_pyarray(py);
                    let nodes = to_torch.call1((nodes,))?;
                    let edges = graph.edges.to_pyarray(py);
                    let edges = to_torch.call1((edges,))?;
                    let edges = to_undirected.call((edges,), None)?;

                    let data_args = PyDict::new(py);
                    data_args.set_item("x", nodes)?;
                    data_args.set_item("edge_index", edges)?;
                    data_args.set_item("num_nodes", node_len)?;

                    let data = new_data.call((), Some(data_args))?;
                    data_list.push(data);
                }

                let batch = new_batch.call1((PyList::new(py, data_list.into_iter()),))?;
                let batch = batch.getattr("cuda")?.call((), None)?;

                // inference
                let x = batch.getattr("x")?;
                let edge_index = batch.getattr("edge_index")?;
                let batch_tensor = batch.getattr("batch")?;

                no_grad.call_method0("__enter__")?;
                let out =
                    model
                        .getattr(py, "forward")?
                        .call(py, (x, edge_index, batch_tensor), None)?;
                no_grad.call_method1("__exit__", (&py.None(), &py.None(), &py.None()))?;

                let out: &PyTuple = out.extract(py)?;
                let v = out.get_item(0);
                let p = out.get_item(1);

                let v = v
                    .getattr("cpu")?
                    .call0()?
                    .getattr("detach")?
                    .call0()?
                    .getattr("numpy")?
                    .call0()?;
                let p = p
                    .getattr("cpu")?
                    .call0()?
                    .getattr("detach")?
                    .call0()?
                    .getattr("numpy")?
                    .call0()?;

                let v: &PyArray<f32, IxDyn> = v.extract()?;
                let p: &PyArray<f32, IxDyn> = p.extract()?;

                let v = v.readonly().to_owned_array();
                let p = p.readonly().to_owned_array();

                Ok((v, p))
            }) {
                Ok(returns) => Ok(returns),
                Err(e) => {
                    Python::with_gil(|py| PyResult::Ok(e.print(py)))?;
                    Err(e)
                }
            }?;

            // unstack the batch
            let mut responses = vec![];
            let mut last_end = 0;
            for (i, len, candidate_node_indexes) in
                izip![0.., nodes_lens.iter(), candidate_node_indexeses]
            {
                let new_start = last_end;
                let new_end = len + last_end;
                last_end = new_end;

                let value = v[[i]];
                let policy = p.slice(s![new_start..new_end]);

                let candidate_node_indexes: HashMap<Action, usize> = candidate_node_indexes;
                let mut policy_map = HashMap::new();
                for (action, index) in candidate_node_indexes {
                    policy_map.insert(action, policy[index]);
                }

                responses.push(EvalOutput { value, policy_map })
            }

            Ok(responses)
        })
        .await??;

        responder.respond(requesters, responses).await?;

        Result::<()>::Ok(())
    }
}

impl EvalClient {
    pub async fn evaluate(&mut self, state: Board) -> EvalOutput {
        self.client.request(state).await;
        self.client.get_response().await
    }
}

#[derive(Debug)]
pub struct TrainHost {
    model: PyObject,
    opt: PyObject,
    gen: u64,
}

impl TrainHost {
    pub fn load(path: impl AsRef<str>) -> Result<Self> {
        let (model, opt) = Python::with_gil(|py| -> PyResult<_> {
            let syspath: &PyList = py.import("sys")?.getattr("path")?.try_into()?;
            syspath.insert(0, "./")?;

            let model = PyModule::import(py, "model")?.getattr("Model")?;

            let model = model.call0()?;
            let model = model.getattr("to")?.call(("cuda",), None)?;

            let params = model.call_method0("parameters")?;
            let opt = PyModule::import(py, "torch.optim")?.getattr("Adam")?.call(
                (params,),
                Some(
                    &hashmap! {
                        "lr" => 1e-5
                    }
                    .into_py_dict(py),
                ),
            )?;

            info!("created model");

            Ok((model.to_object(py), opt.to_object(py)))
        })?;

        let mut gen: u64 = 0;
        if Path::new(path.as_ref()).is_file() {
            Python::with_gil(|py| -> PyResult<()> {
                let torch_load = PyModule::import(py, "torch")?.getattr("load")?;

                let checkpoint = torch_load.call1((path.as_ref(),))?;
                let model_state_dict = checkpoint.get_item("model_state_dict")?;
                let opt_state_dict = checkpoint.get_item("opt_state_dict")?;
                gen = checkpoint.get_item("gen")?.extract()?;

                info!("Loading model gen {}", gen);

                model.call_method1(py, "load_state_dict", (model_state_dict,))?;
                opt.call_method1(py, "load_state_dict", (opt_state_dict,))?;
                Ok(())
            })?;
        } else {
            info!("No model to be loaded, starting anew");
        }

        Ok(Self { model, opt, gen })
    }

    pub fn train(&self, samples: &[Sample], epochs: usize) -> Result<()> {
        Python::with_gil(|py| -> PyResult<()> {
            let syspath: &PyList = py.import("sys")?.getattr("path")?.try_into()?;
            syspath.insert(0, "./")?;

            let train_batch = PyModule::import(py, "model")?.getattr("train")?;
            let to_undirected =
                PyModule::import(py, "torch_geometric.utils")?.getattr("to_undirected")?;
            let to_torch = PyModule::import(py, "torch")?.getattr("from_numpy")?;
            let new_data = PyModule::import(py, "torch_geometric.data")?.getattr("Data")?;

            info!("Converting samples to data list");

            let mut data_list = vec![];
            for sample in samples.iter() {
                let Sample {
                    graph,
                    policy,
                    value,
                } = sample;

                let nodes = graph.nodes.to_pyarray(py);
                let nodes = to_torch.call1((nodes,))?;

                let edges = graph.edges.to_pyarray(py);
                let edges = to_torch.call1((edges,))?;
                let edges = to_undirected.call((edges,), None)?;

                let policy = policy.to_pyarray(py);
                let policy = to_torch.call1((policy,))?;

                let value = value.to_pyarray(py);
                let value = to_torch.call1((value,))?;

                let data_args = PyDict::new(py);
                data_args.set_item("x", nodes)?;
                data_args.set_item("edge_index", edges)?;
                data_args.set_item("num_nodes", graph.num_nodes())?;
                data_args.set_item("policy", policy)?;
                data_args.set_item("value", value)?;

                let data = new_data.call((), Some(data_args))?;
                data_list.push(data);
            }

            for e in 0..epochs {
                info!("Training epoch {}", e);
                let losses: &PyArray<f64, IxDyn> = train_batch
                    .call1((self.model.clone(), self.opt.clone(), data_list.clone()))?
                    .extract()?;
                let losses = losses.to_owned_array();
                info!("loss {} = (v) {} + (p) {}", losses[0], losses[1], losses[2]);
            }

            Ok(())
        })?;
        Ok(())
    }

    pub fn save(&self, path: impl AsRef<str>) -> Result<()> {
        Python::with_gil(|py| -> PyResult<()> {
            let save = PyModule::import(py, "torch")?.getattr("save")?;

            let model_state_dict = self.model.call_method0(py, "state_dict")?;
            let opt_state_dict = self.opt.call_method0(py, "state_dict")?;

            let save_dict = PyDict::new(py);
            save_dict.set_item("model_state_dict", model_state_dict)?;
            save_dict.set_item("opt_state_dict", opt_state_dict)?;
            save_dict.set_item("gen", self.gen + 1)?;
            save.call1((save_dict, path.as_ref()))?;

            Ok(())
        })?;

        Ok(())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Sample {
    pub graph: Graph,
    pub policy: ArrayD<f32>,
    pub value: ArrayD<f32>,
}
