use ccs_gnn::convert::graph_from_board;
use criterion::{black_box, criterion_group, criterion_main, Criterion};

use rand::prelude::*;

use ccs::Board;

pub fn criterion_benchmark(c: &mut Criterion) {
    let mut boards = vec![];
    while boards.len() < 10000 {
        let mut board = Board::new();

        while board.is_ended() == false {
            let mut draw_times = 0;
            loop {
                board.draw();
                let legal_actions = board.legal_actions();
                if legal_actions.is_empty() == false {
                    break;
                }

                draw_times += 1;
                if draw_times >= 1000 {
                    panic!("All remaining tiles have no legal positions");
                }
            }
            let action = *board.legal_actions().choose(&mut thread_rng()).unwrap();

            boards.push(board.clone());
            board.do_action(action);
        }
    }

    c.bench_function("Board-graph conversion (all)", |b| {
        b.iter(|| {
            let board = boards.choose(&mut thread_rng()).unwrap().clone();
            black_box(graph_from_board(board));
        })
    });

    {
        boards = boards.clone();
        boards.retain(|board| board.deck_iter().count() <= 24);

        c.bench_function("Board-graph conversion (late game)", |b| {
            b.iter(|| {
                let board = boards.choose(&mut thread_rng()).unwrap().clone();
                black_box(graph_from_board(board));
            })
        });
    }

    {
        boards = boards.clone();
        let avglen = boards
            .iter()
            .map(|board| board.legal_actions().len())
            .sum::<usize>()
            / boards.len();
        boards.retain(|board| board.legal_actions().len() <= avglen);

        c.bench_function("Board-graph conversion (high action count)", |b| {
            b.iter(|| {
                let board = boards.choose(&mut thread_rng()).unwrap().clone();
                black_box(graph_from_board(board));
            })
        });
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
