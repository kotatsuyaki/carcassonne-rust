{ pkgs ? import <nixpkgs> {
    # Rust overlay
    overlays = [
      (import (builtins.fetchTarball {
        url = https://github.com/oxalica/rust-overlay/archive/master.tar.gz;
      }))
    ];
    config.allowUnfree = true;
  }
, lib ? pkgs.stdenv.lib
}:

let
  unstable = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/nixos-unstable.tar.gz") { };
in
pkgs.mkShell rec {
  buildInputs = with pkgs; [
    # grpc
    protobuf
    # general auto-completion
    tabnine
    # python dev
    python3
    pyright
    # rust dev
    unstable.rust-analyzer
    cargo-watch
    llvmPackages.lld
    sccache
    jemalloc
    # rust
    (rust-bin.stable.latest.default.override {
      extensions = [ "rust-src" ];
    })
  ];

  RUSTFLAGS = "-C link-arg=-fuse-ld=lld";
  RUSTC_WRAPPER = "${pkgs.sccache}/bin/sccache";
}
