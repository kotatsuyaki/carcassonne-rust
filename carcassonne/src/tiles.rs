use super::{Feature, FeatureId, Rotation};
use num_enum::IntoPrimitive;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
#[allow(non_upper_case_globals)]
const c: Feature = Feature::City;
#[allow(non_upper_case_globals)]
const f: Feature = Feature::Field;
#[allow(non_upper_case_globals)]
const r: Feature = Feature::Road;

#[derive(
    Debug, Copy, Clone, Eq, PartialEq, EnumIter, IntoPrimitive, Serialize, Deserialize, Hash,
)]
#[repr(u8)]
pub enum TileKind {
    CastleCenterPennant = 0,
    CastleCenterEntry = 1,
    CastleCenterSide = 2,
    CastleEdge = 3,
    CastleEdgeRoad = 4,
    CastleSides = 5,
    CastleSidesEdge = 6,
    CastleTube = 7,
    CastleWall = 8,
    CastleWallCurveLeft = 9,
    CastleWallCurveRight = 10,
    CastleWallJunction = 11,
    CastleWallRoad = 12,
    Cloister = 13,
    CloisterRoad = 14,
    Road = 15,
    RoadCurve = 16,
    RoadJunctionLarge = 17,
    RoadJunctionSmall = 18,
    CastleCenterEntryPennant = 19,
    CastleCenterSidePennant = 20,
    CastleEdgePennant = 21,
    CastleEdgeRoadPennant = 22,
    CastleTubePennant = 23,
}
impl TileKind {
    pub(crate) fn num_features(&self) -> usize {
        MAX_FEATURE_ID[*self as u8 as usize] + 1
    }
    pub(crate) fn num_tiles(&self) -> usize {
        NUM_TILES[*self as u8 as usize]
    }
    pub(crate) fn feature_ids(&self) -> impl Iterator<Item = FeatureId> {
        FeatureId::iter().take(self.num_features())
    }
    pub(crate) fn allowed_rotations(&self) -> impl Iterator<Item = Rotation> {
        ALLOWED_ROTATIONS[*self as u8 as usize]
            .iter()
            .map(|&num| Rotation::from_u8(num).unwrap())
    }
}
pub(crate) static TILE_IMAGE_BYTES: [&[u8]; 24] = [
    include_bytes!("../../resources/tiles/CastleCenterPennant.png"),
    include_bytes!("../../resources/tiles/CastleCenterEntry.png"),
    include_bytes!("../../resources/tiles/CastleCenterSide.png"),
    include_bytes!("../../resources/tiles/CastleEdge.png"),
    include_bytes!("../../resources/tiles/CastleEdgeRoad.png"),
    include_bytes!("../../resources/tiles/CastleSides.png"),
    include_bytes!("../../resources/tiles/CastleSidesEdge.png"),
    include_bytes!("../../resources/tiles/CastleTube.png"),
    include_bytes!("../../resources/tiles/CastleWall.png"),
    include_bytes!("../../resources/tiles/CastleWallCurveLeft.png"),
    include_bytes!("../../resources/tiles/CastleWallCurveRight.png"),
    include_bytes!("../../resources/tiles/CastleWallJunction.png"),
    include_bytes!("../../resources/tiles/CastleWallRoad.png"),
    include_bytes!("../../resources/tiles/Cloister.png"),
    include_bytes!("../../resources/tiles/CloisterRoad.png"),
    include_bytes!("../../resources/tiles/Road.png"),
    include_bytes!("../../resources/tiles/RoadCurve.png"),
    include_bytes!("../../resources/tiles/RoadJunctionLarge.png"),
    include_bytes!("../../resources/tiles/RoadJunctionSmall.png"),
    include_bytes!("../../resources/tiles/CastleCenterEntryPennant.png"),
    include_bytes!("../../resources/tiles/CastleCenterSidePennant.png"),
    include_bytes!("../../resources/tiles/CastleEdgePennant.png"),
    include_bytes!("../../resources/tiles/CastleEdgeRoadPennant.png"),
    include_bytes!("../../resources/tiles/CastleTubePennant.png"),
];
pub(crate) static EDGE_FEATURES: [[[Feature; 12]; 4]; 24] = [
    [
        // (0) CastleCenterPennant
        [c, c, c, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, c, c, c],
    ],
    [
        // (1) CastleCenterEntry
        [c, c, c, c, c, c, f, r, f, c, c, c],
        [c, c, c, f, r, f, c, c, c, c, c, c],
        [f, r, f, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, f, r, f],
    ],
    [
        // (2) CastleCenterSide
        [c, c, c, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, c, c, c],
        [f, f, f, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, f, f, f],
    ],
    [
        // (3) CastleEdge
        [c, c, c, c, c, c, f, f, f, f, f, f],
        [c, c, c, f, f, f, f, f, f, c, c, c],
        [f, f, f, f, f, f, c, c, c, c, c, c],
        [f, f, f, c, c, c, c, c, c, f, f, f],
    ],
    [
        // (4) CastleEdgeRoad
        [c, c, c, c, c, c, f, r, f, f, r, f],
        [c, c, c, f, r, f, f, r, f, c, c, c],
        [f, r, f, f, r, f, c, c, c, c, c, c],
        [f, r, f, c, c, c, c, c, c, f, r, f],
    ],
    [
        // (5) CastleSides
        [c, c, c, f, f, f, c, c, c, f, f, f],
        [f, f, f, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, f, f, f],
        [f, f, f, c, c, c, f, f, f, c, c, c],
    ],
    [
        // (6) CastleSidesEdge
        [c, c, c, f, f, f, f, f, f, c, c, c],
        [f, f, f, f, f, f, c, c, c, c, c, c],
        [f, f, f, c, c, c, c, c, c, f, f, f],
        [c, c, c, c, c, c, f, f, f, f, f, f],
    ],
    [
        // (7) CastleTube
        [f, f, f, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, f, f, f],
        [f, f, f, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, f, f, f],
    ],
    [
        // (8) CastleWall
        [c, c, c, f, f, f, f, f, f, f, f, f],
        [f, f, f, f, f, f, f, f, f, c, c, c],
        [f, f, f, f, f, f, c, c, c, f, f, f],
        [f, f, f, c, c, c, f, f, f, f, f, f],
    ],
    [
        // (9) CastleWallCurveLeft
        [c, c, c, f, f, f, f, r, f, f, r, f],
        [f, f, f, f, r, f, f, r, f, c, c, c],
        [f, r, f, f, r, f, c, c, c, f, f, f],
        [f, r, f, c, c, c, f, f, f, f, r, f],
    ],
    [
        // (10) CastleWallCurveRight
        [c, c, c, f, r, f, f, r, f, f, f, f],
        [f, r, f, f, r, f, f, f, f, c, c, c],
        [f, r, f, f, f, f, c, c, c, f, r, f],
        [f, f, f, c, c, c, f, r, f, f, r, f],
    ],
    [
        // (11) CastleWallJunction
        [c, c, c, f, r, f, f, r, f, f, r, f],
        [f, r, f, f, r, f, f, r, f, c, c, c],
        [f, r, f, f, r, f, c, c, c, f, r, f],
        [f, r, f, c, c, c, f, r, f, f, r, f],
    ],
    [
        // (12) CastleWallRoad
        [c, c, c, f, r, f, f, f, f, f, r, f],
        [f, r, f, f, f, f, f, r, f, c, c, c],
        [f, f, f, f, r, f, c, c, c, f, r, f],
        [f, r, f, c, c, c, f, r, f, f, f, f],
    ],
    [
        // (13) Cloister
        [f, f, f, f, f, f, f, f, f, f, f, f],
        [f, f, f, f, f, f, f, f, f, f, f, f],
        [f, f, f, f, f, f, f, f, f, f, f, f],
        [f, f, f, f, f, f, f, f, f, f, f, f],
    ],
    [
        // (14) CloisterRoad
        [f, f, f, f, f, f, f, r, f, f, f, f],
        [f, f, f, f, r, f, f, f, f, f, f, f],
        [f, r, f, f, f, f, f, f, f, f, f, f],
        [f, f, f, f, f, f, f, f, f, f, r, f],
    ],
    [
        // (15) Road
        [f, r, f, f, f, f, f, r, f, f, f, f],
        [f, f, f, f, r, f, f, f, f, f, r, f],
        [f, r, f, f, f, f, f, r, f, f, f, f],
        [f, f, f, f, r, f, f, f, f, f, r, f],
    ],
    [
        // (16) RoadCurve
        [f, f, f, f, f, f, f, r, f, f, r, f],
        [f, f, f, f, r, f, f, r, f, f, f, f],
        [f, r, f, f, r, f, f, f, f, f, f, f],
        [f, r, f, f, f, f, f, f, f, f, r, f],
    ],
    [
        // (17) RoadJunctionLarge
        [f, r, f, f, r, f, f, r, f, f, r, f],
        [f, r, f, f, r, f, f, r, f, f, r, f],
        [f, r, f, f, r, f, f, r, f, f, r, f],
        [f, r, f, f, r, f, f, r, f, f, r, f],
    ],
    [
        // (18) RoadJunctionSmall
        [f, f, f, f, r, f, f, r, f, f, r, f],
        [f, r, f, f, r, f, f, r, f, f, f, f],
        [f, r, f, f, r, f, f, f, f, f, r, f],
        [f, r, f, f, f, f, f, r, f, f, r, f],
    ],
    [
        // (19) CastleCenterEntryPennant
        [c, c, c, c, c, c, f, r, f, c, c, c],
        [c, c, c, f, r, f, c, c, c, c, c, c],
        [f, r, f, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, f, r, f],
    ],
    [
        // (20) CastleCenterSidePennant
        [c, c, c, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, c, c, c],
        [f, f, f, c, c, c, c, c, c, c, c, c],
        [c, c, c, c, c, c, c, c, c, f, f, f],
    ],
    [
        // (21) CastleEdgePennant
        [c, c, c, c, c, c, f, f, f, f, f, f],
        [c, c, c, f, f, f, f, f, f, c, c, c],
        [f, f, f, f, f, f, c, c, c, c, c, c],
        [f, f, f, c, c, c, c, c, c, f, f, f],
    ],
    [
        // (22) CastleEdgeRoadPennant
        [c, c, c, c, c, c, f, r, f, f, r, f],
        [c, c, c, f, r, f, f, r, f, c, c, c],
        [f, r, f, f, r, f, c, c, c, c, c, c],
        [f, r, f, c, c, c, c, c, c, f, r, f],
    ],
    [
        // (23) CastleTubePennant
        [f, f, f, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, f, f, f],
        [f, f, f, c, c, c, f, f, f, c, c, c],
        [c, c, c, f, f, f, c, c, c, f, f, f],
    ],
];
pub(crate) static FEATURE_IDS: [[[u8; 12]; 4]; 24] = [
    [
        // (0) CastleCenterPennant
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    [
        // (1) CastleCenterEntry
        [0, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 0],
        [0, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0],
        [1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3],
    ],
    [
        // (2) CastleCenterSide
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    ],
    [
        // (3) CastleEdge
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    ],
    [
        // (4) CastleEdgeRoad
        [0, 0, 0, 0, 0, 0, 1, 2, 3, 3, 2, 1],
        [0, 0, 0, 1, 2, 3, 3, 2, 1, 0, 0, 0],
        [1, 2, 3, 3, 2, 1, 0, 0, 0, 0, 0, 0],
        [3, 2, 1, 0, 0, 0, 0, 0, 0, 1, 2, 3],
    ],
    [
        // (5) CastleSides
        [0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1],
        [1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0],
        [2, 2, 2, 1, 1, 1, 0, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 2],
    ],
    [
        // (6) CastleSidesEdge
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 2, 2, 2],
        [1, 1, 1, 1, 1, 1, 2, 2, 2, 0, 0, 0],
        [1, 1, 1, 2, 2, 2, 0, 0, 0, 1, 1, 1],
        [2, 2, 2, 0, 0, 0, 1, 1, 1, 1, 1, 1],
    ],
    [
        // (7) CastleTube
        [0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1],
        [1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0],
        [2, 2, 2, 1, 1, 1, 0, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 2],
    ],
    [
        // (8) CastleWall
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1],
        [1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1],
    ],
    [
        // (9) CastleWallCurveLeft
        [0, 0, 0, 1, 1, 1, 1, 2, 3, 3, 2, 1],
        [1, 1, 1, 1, 2, 3, 3, 2, 1, 0, 0, 0],
        [1, 2, 3, 3, 2, 1, 0, 0, 0, 1, 1, 1],
        [3, 2, 1, 0, 0, 0, 1, 1, 1, 1, 2, 3],
    ],
    [
        // (10) CastleWallCurveRight
        [0, 0, 0, 1, 2, 3, 3, 2, 1, 1, 1, 1],
        [1, 2, 3, 3, 2, 1, 1, 1, 1, 0, 0, 0],
        [3, 2, 1, 1, 1, 1, 0, 0, 0, 1, 2, 3],
        [1, 1, 1, 0, 0, 0, 1, 2, 3, 3, 2, 1],
    ],
    [
        // (11) CastleWallJunction
        [0, 0, 0, 1, 2, 3, 3, 4, 5, 5, 6, 1],
        [1, 2, 3, 3, 4, 5, 5, 6, 1, 0, 0, 0],
        [3, 4, 5, 5, 6, 1, 0, 0, 0, 1, 2, 3],
        [5, 6, 1, 0, 0, 0, 1, 2, 3, 3, 4, 5],
    ],
    [
        // (12) CastleWallRoad
        [0, 0, 0, 1, 2, 3, 3, 3, 3, 3, 2, 1],
        [1, 2, 3, 3, 3, 3, 3, 2, 1, 0, 0, 0],
        [3, 3, 3, 3, 2, 1, 0, 0, 0, 1, 2, 3],
        [3, 2, 1, 0, 0, 0, 1, 2, 3, 3, 3, 3],
    ],
    [
        // (13) Cloister
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    ],
    [
        // (14) CloisterRoad
        [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0],
    ],
    [
        // (15) Road
        [0, 1, 2, 2, 2, 2, 2, 1, 0, 0, 0, 0],
        [2, 2, 2, 2, 1, 0, 0, 0, 0, 0, 1, 2],
        [2, 1, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2],
        [0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 1, 0],
    ],
    [
        // (16) RoadCurve
        [0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 1, 0],
        [0, 0, 0, 0, 1, 2, 2, 1, 0, 0, 0, 0],
        [0, 1, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0],
        [2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2],
    ],
    [
        // (17) RoadJunctionLarge
        [0, 1, 2, 2, 3, 4, 4, 5, 6, 6, 7, 0],
        [2, 3, 4, 4, 5, 6, 6, 7, 0, 0, 1, 2],
        [4, 5, 6, 6, 7, 0, 0, 1, 2, 2, 3, 4],
        [6, 7, 0, 0, 1, 2, 2, 3, 4, 4, 5, 6],
    ],
    [
        // (18) RoadJunctionSmall
        [0, 0, 0, 0, 1, 2, 2, 3, 4, 4, 5, 0],
        [0, 1, 2, 2, 3, 4, 4, 5, 0, 0, 0, 0],
        [2, 3, 4, 4, 5, 0, 0, 0, 0, 0, 1, 2],
        [4, 5, 0, 0, 0, 0, 0, 1, 2, 2, 3, 4],
    ],
    [
        // (19) CastleCenterEntryPennant
        [0, 0, 0, 0, 0, 0, 1, 2, 3, 0, 0, 0],
        [0, 0, 0, 1, 2, 3, 0, 0, 0, 0, 0, 0],
        [1, 2, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 3],
    ],
    [
        // (20) CastleCenterSidePennant
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0],
        [0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    ],
    [
        // (21) CastleEdgePennant
        [0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1],
        [0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0],
        [1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0],
        [1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1],
    ],
    [
        // (22) CastleEdgeRoadPennant
        [0, 0, 0, 0, 0, 0, 1, 2, 3, 3, 2, 1],
        [0, 0, 0, 1, 2, 3, 3, 2, 1, 0, 0, 0],
        [1, 2, 3, 3, 2, 1, 0, 0, 0, 0, 0, 0],
        [3, 2, 1, 0, 0, 0, 0, 0, 0, 1, 2, 3],
    ],
    [
        // (23) CastleTubePennant
        [0, 0, 0, 1, 1, 1, 2, 2, 2, 1, 1, 1],
        [1, 1, 1, 2, 2, 2, 1, 1, 1, 0, 0, 0],
        [2, 2, 2, 1, 1, 1, 0, 0, 0, 1, 1, 1],
        [1, 1, 1, 0, 0, 0, 1, 1, 1, 2, 2, 2],
    ],
];
pub(crate) static MAX_FEATURE_ID: [usize; 24] = [
    0, // (0) CastleCenterPennant
    3, // (1) CastleCenterEntry
    1, // (2) CastleCenterSide
    1, // (3) CastleEdge
    3, // (4) CastleEdgeRoad
    2, // (5) CastleSides
    2, // (6) CastleSidesEdge
    2, // (7) CastleTube
    1, // (8) CastleWall
    3, // (9) CastleWallCurveLeft
    3, // (10) CastleWallCurveRight
    6, // (11) CastleWallJunction
    3, // (12) CastleWallRoad
    0, // (13) Cloister
    1, // (14) CloisterRoad
    2, // (15) Road
    2, // (16) RoadCurve
    7, // (17) RoadJunctionLarge
    5, // (18) RoadJunctionSmall
    3, // (19) CastleCenterEntryPennant
    1, // (20) CastleCenterSidePennant
    1, // (21) CastleEdgePennant
    3, // (22) CastleEdgeRoadPennant
    2, // (23) CastleTubePennant
];
pub(crate) static ALLOWED_ROTATIONS: [&[u8]; 24] = [
    &[0],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1],
    &[0, 1, 2, 3],
    &[0, 1],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0],
    &[0, 1, 2, 3],
    &[0, 1],
    &[0, 1, 2, 3],
    &[0],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1, 2, 3],
    &[0, 1],
];
pub(crate) static FIELD_FEATURE_ADJ_CITIES: [[[u8; 2]; 8]; 24] = [
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 1],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 2],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [1, 0],
        [0, 0],
        [1, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
    [
        [1, 0],
        [1, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
        [0, 0],
    ],
];
pub(crate) static FIELD_FEATURE_NUM_ADJ_CITIES: [[usize; 8]; 24] = [
    [0, 0, 0, 0, 0, 0, 0, 0], // (0) CastleCenterPennant
    [0, 1, 0, 1, 0, 0, 0, 0], // (1) CastleCenterEntry
    [0, 1, 0, 0, 0, 0, 0, 0], // (2) CastleCenterSide
    [0, 1, 0, 0, 0, 0, 0, 0], // (3) CastleEdge
    [0, 1, 0, 0, 0, 0, 0, 0], // (4) CastleEdgeRoad
    [0, 2, 0, 0, 0, 0, 0, 0], // (5) CastleSides
    [0, 2, 0, 0, 0, 0, 0, 0], // (6) CastleSidesEdge
    [1, 0, 1, 0, 0, 0, 0, 0], // (7) CastleTube
    [0, 1, 0, 0, 0, 0, 0, 0], // (8) CastleWall
    [0, 1, 0, 0, 0, 0, 0, 0], // (9) CastleWallCurveLeft
    [0, 1, 0, 0, 0, 0, 0, 0], // (10) CastleWallCurveRight
    [0, 1, 0, 0, 0, 0, 0, 0], // (11) CastleWallJunction
    [0, 1, 0, 0, 0, 0, 0, 0], // (12) CastleWallRoad
    [0, 0, 0, 0, 0, 0, 0, 0], // (13) Cloister
    [0, 0, 0, 0, 0, 0, 0, 0], // (14) CloisterRoad
    [0, 0, 0, 0, 0, 0, 0, 0], // (15) Road
    [0, 0, 0, 0, 0, 0, 0, 0], // (16) RoadCurve
    [0, 0, 0, 0, 0, 0, 0, 0], // (17) RoadJunctionLarge
    [0, 0, 0, 0, 0, 0, 0, 0], // (18) RoadJunctionSmall
    [0, 1, 0, 1, 0, 0, 0, 0], // (19) CastleCenterEntryPennant
    [0, 1, 0, 0, 0, 0, 0, 0], // (20) CastleCenterSidePennant
    [0, 1, 0, 0, 0, 0, 0, 0], // (21) CastleEdgePennant
    [0, 1, 0, 0, 0, 0, 0, 0], // (22) CastleEdgeRoadPennant
    [1, 1, 0, 0, 0, 0, 0, 0], // (23) CastleTubePennant
];
pub(crate) static HAS_PENNANT: [[bool; 8]; 24] = [
    [true, false, false, false, false, false, false, false], // (0) CastleCenterPennant
    [false, false, false, false, false, false, false, false], // (1) CastleCenterEntry
    [false, false, false, false, false, false, false, false], // (2) CastleCenterSide
    [false, false, false, false, false, false, false, false], // (3) CastleEdge
    [false, false, false, false, false, false, false, false], // (4) CastleEdgeRoad
    [false, false, false, false, false, false, false, false], // (5) CastleSides
    [false, false, false, false, false, false, false, false], // (6) CastleSidesEdge
    [false, false, false, false, false, false, false, false], // (7) CastleTube
    [false, false, false, false, false, false, false, false], // (8) CastleWall
    [false, false, false, false, false, false, false, false], // (9) CastleWallCurveLeft
    [false, false, false, false, false, false, false, false], // (10) CastleWallCurveRight
    [false, false, false, false, false, false, false, false], // (11) CastleWallJunction
    [false, false, false, false, false, false, false, false], // (12) CastleWallRoad
    [false, false, false, false, false, false, false, false], // (13) Cloister
    [false, false, false, false, false, false, false, false], // (14) CloisterRoad
    [false, false, false, false, false, false, false, false], // (15) Road
    [false, false, false, false, false, false, false, false], // (16) RoadCurve
    [false, false, false, false, false, false, false, false], // (17) RoadJunctionLarge
    [false, false, false, false, false, false, false, false], // (18) RoadJunctionSmall
    [true, false, false, false, false, false, false, false], // (19) CastleCenterEntryPennant
    [true, false, false, false, false, false, false, false], // (20) CastleCenterSidePennant
    [true, false, false, false, false, false, false, false], // (21) CastleEdgePennant
    [true, false, false, false, false, false, false, false], // (22) CastleEdgeRoadPennant
    [false, true, false, false, false, false, false, false], // (23) CastleTubePennant
];
pub(crate) static FEATURE_KINDS: [[Feature; 8]; 24] = [
    [c, f, f, f, f, f, f, f], // (0) CastleCenterPennant
    [c, f, r, f, f, f, f, f], // (1) CastleCenterEntry
    [c, f, f, f, f, f, f, f], // (2) CastleCenterSide
    [c, f, f, f, f, f, f, f], // (3) CastleEdge
    [c, f, r, f, f, f, f, f], // (4) CastleEdgeRoad
    [c, f, c, f, f, f, f, f], // (5) CastleSides
    [c, f, c, f, f, f, f, f], // (6) CastleSidesEdge
    [f, c, f, f, f, f, f, f], // (7) CastleTube
    [c, f, f, f, f, f, f, f], // (8) CastleWall
    [c, f, r, f, f, f, f, f], // (9) CastleWallCurveLeft
    [c, f, r, f, f, f, f, f], // (10) CastleWallCurveRight
    [c, f, r, f, r, f, r, f], // (11) CastleWallJunction
    [c, f, r, f, f, f, f, f], // (12) CastleWallRoad
    [f, f, f, f, f, f, f, f], // (13) Cloister
    [f, r, f, f, f, f, f, f], // (14) CloisterRoad
    [f, r, f, f, f, f, f, f], // (15) Road
    [f, r, f, f, f, f, f, f], // (16) RoadCurve
    [f, r, f, r, f, r, f, r], // (17) RoadJunctionLarge
    [f, r, f, r, f, r, f, f], // (18) RoadJunctionSmall
    [c, f, r, f, f, f, f, f], // (19) CastleCenterEntryPennant
    [c, f, f, f, f, f, f, f], // (20) CastleCenterSidePennant
    [c, f, f, f, f, f, f, f], // (21) CastleEdgePennant
    [c, f, r, f, f, f, f, f], // (22) CastleEdgeRoadPennant
    [f, c, f, f, f, f, f, f], // (23) CastleTubePennant
];
pub(crate) static HAS_CLOISTER: [bool; 24] = [
    false, // (0) CastleCenterPennant
    false, // (1) CastleCenterEntry
    false, // (2) CastleCenterSide
    false, // (3) CastleEdge
    false, // (4) CastleEdgeRoad
    false, // (5) CastleSides
    false, // (6) CastleSidesEdge
    false, // (7) CastleTube
    false, // (8) CastleWall
    false, // (9) CastleWallCurveLeft
    false, // (10) CastleWallCurveRight
    false, // (11) CastleWallJunction
    false, // (12) CastleWallRoad
    true,  // (13) Cloister
    true,  // (14) CloisterRoad
    false, // (15) Road
    false, // (16) RoadCurve
    false, // (17) RoadJunctionLarge
    false, // (18) RoadJunctionSmall
    false, // (19) CastleCenterEntryPennant
    false, // (20) CastleCenterSidePennant
    false, // (21) CastleEdgePennant
    false, // (22) CastleEdgeRoadPennant
    false, // (23) CastleTubePennant
];
pub(crate) static NUM_TILES: [usize; 24] = [
    1, // (0) CastleCenterPennant
    1, // (1) CastleCenterEntry
    3, // (2) CastleCenterSide
    3, // (3) CastleEdge
    3, // (4) CastleEdgeRoad
    3, // (5) CastleSides
    2, // (6) CastleSidesEdge
    1, // (7) CastleTube
    5, // (8) CastleWall
    3, // (9) CastleWallCurveLeft
    3, // (10) CastleWallCurveRight
    3, // (11) CastleWallJunction
    4, // (12) CastleWallRoad
    4, // (13) Cloister
    2, // (14) CloisterRoad
    8, // (15) Road
    9, // (16) RoadCurve
    1, // (17) RoadJunctionLarge
    4, // (18) RoadJunctionSmall
    2, // (19) CastleCenterEntryPennant
    1, // (20) CastleCenterSidePennant
    2, // (21) CastleEdgePennant
    2, // (22) CastleEdgeRoadPennant
    2, // (23) CastleTubePennant
];
