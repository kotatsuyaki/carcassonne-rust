#![allow(clippy::bool_comparison)]

use std::{cmp::Ordering, fs::create_dir_all, io::Cursor, path::Path};

use bitvec::prelude::*;
use guard::guard;
use hashbrown::{HashMap, HashSet};
use image::{
    imageops::{overlay, rotate180, rotate270, rotate90},
    ImageBuffer, Rgba, RgbaImage,
};
use itertools::Itertools;
use num_derive::FromPrimitive;
use num_enum::IntoPrimitive;
use num_traits::FromPrimitive;
use rand::prelude::*;
use serde::{Deserialize, Serialize};
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use tracing::debug;

pub use tiles::TileKind;
use tiles::{
    EDGE_FEATURES, FEATURE_IDS, FEATURE_KINDS, FIELD_FEATURE_ADJ_CITIES,
    FIELD_FEATURE_NUM_ADJ_CITIES, HAS_CLOISTER, HAS_PENNANT, TILE_IMAGE_BYTES,
};

mod tiles;

macro_rules! crd {
    ($i: expr, $j: expr) => {
        Coord {
            i: $i as i8,
            j: $j as i8,
        }
    };
}

/// Cast identifiers and / or expressions as u8, and then cast as usize.
/// This is used to convert the typed indexes (Segment, FeatureId etc.) into array indexes.
///
/// Example:
/// ```
/// // using this macro
/// cast_all!(kind = self.kind, rotation = self.rotation, seg);
///
/// // identical to
/// let kind = self.kind as u8 as usize;
/// let rotation = self.rotation as u8 as usize;
/// let seg = seg as u8 as usize;
/// ```
macro_rules! cast_all {
    () => {};
    ($aft: ident = $bf: expr) => {
        let $aft = $bf as u8 as usize;
    };
    ($aft: ident) => {
        let $aft = $aft as u8 as usize;
    };
    ($aft: ident = $bf: expr, $($rest: tt)*) => {
        let $aft = $bf as u8 as usize;
        cast_all!($($rest)*);
    };
    ($aft: ident, $($rest: tt)*) => {
        let $aft = $aft as u8 as usize;
        cast_all!($($rest)*);
    };
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Board {
    tiles: HashMap<Coord, TileEntry>,
    deck: Vec<TileKind>,
    meeples: HashMap<MeepleCoord, Player>,
    scores: [u8; 2],
    meeples_remain: [u8; 2],
    turn: Player,
    legal_coordinates: HashSet<Coord>,
}

// 3 bytes per entry
#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
struct TileEntry {
    tile: Tile,
    /// Feature id -> the feature is occupied or not (bool)
    #[serde(skip)]
    occupied: BitArr!(for 8, in Msb0, u8),
}

impl TileEntry {
    fn occupied_on_seg(&self, seg: Segment) -> bool {
        let feature_id = self.tile.feature_id_of(seg);
        self.occupied_on_feature(feature_id)
    }

    fn occupied_on_feature(&self, feature_id: FeatureId) -> bool {
        self.occupied[feature_id.i()]
    }

    fn set_occupied(&mut self, feature_id: FeatureId) {
        self.occupied.set(feature_id.i(), true);
    }
}

impl Board {
    pub fn new() -> Self {
        let tiles = HashMap::new();
        let meeples = HashMap::new();
        let deck = TileKind::iter()
            .map(|kind| {
                let mut num_tiles = kind.num_tiles();
                if kind == TileKind::CastleWallRoad {
                    num_tiles -= 1;
                }
                std::iter::repeat(kind).take(num_tiles)
            })
            .flatten()
            .collect_vec();
        let legal_coordinates = [crd!(1, 0), crd!(0, 1), crd!(-1, 0), crd!(0, -1)]
            .iter()
            .cloned()
            .collect();

        let mut board = Board {
            tiles,
            meeples,
            deck,
            scores: [0, 0],
            meeples_remain: [7, 7],
            turn: Player::Red,
            legal_coordinates,
        };

        let start_tile_rotation = Rotation::choose();
        board.place_tile(
            crd!(0, 0),
            Tile {
                kind: TileKind::CastleWallRoad,
                rotation: start_tile_rotation,
            },
        );

        board
    }

    pub fn new_small() -> Self {
        let mut board = Self::new();
        board.deck = TileKind::iter().collect();
        board
    }

    pub fn is_ended(&self) -> bool {
        self.deck.is_empty()
    }

    pub fn scores(&self) -> [u8; 2] {
        self.scores
    }

    pub fn winner(&self) -> Option<Player> {
        let scores = self.scores();
        if scores[0] > scores[1] {
            Some(Player::Red)
        } else if scores[0] < scores[1] {
            Some(Player::Blue)
        } else {
            None
        }
    }

    pub fn meeples_remain(&self) -> [u8; 2] {
        self.meeples_remain
    }

    fn place_tile(&mut self, coord: Coord, tile: Tile) {
        self.tiles.insert(
            coord,
            TileEntry {
                tile,
                occupied: bitarr![Msb0, u8; 0; 8],
            },
        );
    }

    fn place_meeple_on_edge(&mut self, coord: Coord, feature_id: usize) {
        self.meeples
            .insert(MeepleCoord::Edge(coord, feature_id as u8), self.turn);
        self.meeples_remain[self.turn.i()] -= 1;
    }

    fn place_meeple_on_cloister(&mut self, coord: Coord) {
        self.meeples.insert(MeepleCoord::Cloister(coord), self.turn);
        self.meeples_remain[self.turn.i()] -= 1;
    }

    pub fn tile_at(&self, coord: Coord) -> Option<&Tile> {
        self.tiles.get(&coord).map(|entry| &entry.tile)
    }

    fn entry_at(&self, coord: Coord) -> Option<&TileEntry> {
        self.tiles.get(&coord)
    }

    fn entry_at_mut(&mut self, coord: Coord) -> Option<&mut TileEntry> {
        self.tiles.get_mut(&coord)
    }

    pub fn do_action(&mut self, action: Action) {
        let kind = self.deck.pop().expect("do_action called with empty deck");

        let Action {
            coord,
            rotation,
            meeple_action,
        } = action;

        // Place the tile
        let tile = Tile { kind, rotation };
        self.place_tile(coord, tile);

        // Place the meeple
        match meeple_action {
            MeepleAction::Edge(feature_id) => self.place_meeple_on_edge(coord, feature_id as usize),
            MeepleAction::Cloister => self.place_meeple_on_cloister(coord),
            _ => {}
        }

        // update occupied status & do in-game score update for cities / roads
        for feature_id in kind.feature_ids() {
            let feature_kind = tile.feature_kind_on(feature_id);

            // whether to propagate "occupied" or not
            let is_meeple_placed_on_this_feature =
                meeple_action == MeepleAction::Edge(feature_id as u8);
            let should_set_occupied =
                is_meeple_placed_on_this_feature || self.has_occupied_neighbor(coord, feature_id);

            // skip non-occupied features, to avoid running floodfill on it
            if should_set_occupied == false {
                continue;
            }

            // get connected feature area information
            let floodfill_output = self.floodfill(FloodfillConfig {
                should_set_occupied,
                from_coord: coord,
                from_feature_id: feature_id,
            });

            // in-game score update for cities & roads
            // condition: (is city OR is road) AND closed
            if matches!(feature_kind, Feature::City | Feature::Road) && floodfill_output.is_closed {
                let FloodfillOutput {
                    ref visited,
                    pennant_count,
                    ..
                } = floodfill_output;

                let score_multiplier = if feature_kind == Feature::City { 2 } else { 1 };

                self.update_scores(UpdateScoreConfig {
                    visited,
                    pennant_count,
                    score_multiplier,
                    should_recycle_meeples: true,
                });
            }
        }

        // do in-game score update for cloisters
        for &adj_coord in coord.eight_adjacents().iter() {
            if let Some(player) = self.get_meeple_on_cloister(adj_coord) {
                let is_completed = self.count_tiles_around(adj_coord) == 8;
                if is_completed {
                    self.meeples.remove(&MeepleCoord::Cloister(adj_coord));
                    self.meeples_remain[player.i()] += 1;
                    self.scores[player.i()] += 9;
                }
            }
        }

        if self.deck.is_empty() {
            self.endgame_score_update();
        }

        // update legal coordinates (remove used place & add its surrounding places)
        self.legal_coordinates.remove(&coord);
        for (_side, neighbor) in coord.neighbors() {
            if self.tile_at(neighbor).is_none() {
                self.legal_coordinates.insert(neighbor);
            }
        }

        self.turn = self.turn.opposite();
    }

    fn count_tiles_around(&self, coord: Coord) -> usize {
        coord
            .eight_adjacents()
            .iter()
            .filter(|&&coord| self.tile_at(coord).is_some())
            .count()
    }

    fn get_meeple_on_edge(&self, coord: Coord, feature_id: FeatureId) -> Option<Player> {
        self.meeples
            .get(&MeepleCoord::Edge(coord, feature_id as u8))
            .cloned()
    }

    fn get_meeple_on_cloister(&self, coord: Coord) -> Option<Player> {
        self.meeples.get(&MeepleCoord::Cloister(coord)).cloned()
    }

    pub fn legal_actions(&self) -> Vec<Action> {
        let kind = *self.deck.last().unwrap();

        // get legal coordinates
        let avail = self.legal_coordinates();

        // try every rotation and meeple placement
        let mut actions = vec![];
        for coord in avail {
            for rotation in kind.allowed_rotations() {
                let tile = Tile { rotation, kind };

                // check if all border matches
                let border_matches = self.border_matches(tile, coord);
                if border_matches == false {
                    continue;
                }

                /* now the border all matches */
                // push the plain action without placing meeple
                actions.push(Action {
                    coord,
                    rotation,
                    meeple_action: MeepleAction::None,
                });

                let still_has_meeple = self.meeples_remain[self.turn.i()] != 0;
                if still_has_meeple {
                    // 1. check how we can place a meeple on the edges

                    // indexed with feature id
                    // initialized as [true] * number of features
                    let mut is_meeple_allowed = [true; 8];

                    // for every segment of this tile, check if the opposite segment is occupied
                    for seg in Segment::iter() {
                        let feature_id = tile.feature_id_of(seg);

                        // early return
                        if is_meeple_allowed[feature_id.i()] == false {
                            continue;
                        }

                        // only check occupied status if there's an adjacent tile entry
                        if let Some(neighbor) = self.entry_at(coord.neighbor_on(seg.side())) {
                            if neighbor.occupied_on_seg(seg.opposite()) {
                                is_meeple_allowed[feature_id.i()] = false;
                            }
                        }
                    } // for seg

                    // is_meeple_allowed now contains feature ids that are allowed
                    for feature_id in kind.feature_ids() {
                        if is_meeple_allowed[feature_id.i()] {
                            actions.push(Action {
                                coord,
                                rotation,
                                meeple_action: MeepleAction::Edge(feature_id as u8),
                            });
                        }
                    } // for feature id

                    // 2. check if we can place a meeple on the cloister
                    if tile.has_cloister() {
                        actions.push(Action {
                            coord,
                            rotation,
                            meeple_action: MeepleAction::Cloister,
                        });
                    }
                } // if still_has_meeple
            } // for rotation
        } // for available coord
        actions
    }

    pub fn legal_draws(&self) -> Vec<(TileKind, usize)> {
        let mut counts = HashMap::new();
        for &kind in self.deck.iter() {
            *counts.entry(kind).or_insert(0) += 1;
        }
        counts.into_iter().collect_vec()
    }

    pub fn do_draw(&mut self, kind: TileKind) {
        let index = self
            .deck
            .iter()
            .position(|&deck_kind| deck_kind == kind)
            .unwrap();
        let len = self.deck.len();
        self.deck.swap(index, len - 1);
    }

    fn legal_coordinates(&self) -> impl Iterator<Item = Coord> + '_ {
        self.legal_coordinates.iter().cloned()
    }

    fn border_matches(&self, tile: Tile, coord: Coord) -> bool {
        coord.neighbors().iter().all(|(side, neighbor_coord)| {
            if let Some(neighbor_tile) = self.tile_at(*neighbor_coord) {
                // get border features of the two tiles
                let self_side_features = tile.side_features(*side);
                let neighbor_side_features = neighbor_tile.side_features(side.opposite());

                self_side_features == neighbor_side_features
            } else {
                // open border always matches
                true
            }
        })
    }

    /// Draw a tile by randomly swapping a tile to the last place.
    pub fn draw(&mut self) {
        assert!(
            !self.deck.is_empty(),
            "Board::draw called with empty `deck`"
        );

        let len = self.deck.len();
        let idx = thread_rng().gen_range(0..len);
        self.deck.swap(idx, len - 1);
    }

    /// Create and save image representation and save to `path`.
    /// The containing directory of `path` is automatically created if it doesn't exist.
    pub fn save_image(&self, path: impl AsRef<Path>) {
        let coords = self.tiles.keys().collect_vec();
        let min_i = coords.iter().min_by_key(|c| c.i).unwrap().i as i32;
        let max_i = coords.iter().max_by_key(|c| c.i).unwrap().i as i32;
        let min_j = coords.iter().min_by_key(|c| c.j).unwrap().j as i32;
        let max_j = coords.iter().max_by_key(|c| c.j).unwrap().j as i32;
        let height = ((max_i - min_i + 1) * 64) as u32;
        let width = ((max_j - min_j + 1) * 64) as u32;

        let mut buf = ImageBuffer::from_pixel(width, height, Rgba([255, 255, 255, 255]));

        for (coord, entry) in self.tiles.iter() {
            let TileEntry { tile, .. } = entry;
            let i = (coord.i as i32 - min_i) as u32;
            let j = (coord.j as i32 - min_j) as u32;

            let mut tile_img = tile.image();

            // draw edge meeples
            let maybe_meeple = tile.kind.feature_ids().find_map(|feature_id| {
                self.get_meeple_on_edge(*coord, feature_id)
                    .map(|player| (feature_id, player))
            });
            if let Some((feature_id, player)) = maybe_meeple {
                let seg = Segment::iter()
                    .find(|seg| tile.feature_id_of(*seg) == feature_id)
                    .unwrap();

                let meeple_bytes = player.meeple_image_bytes();
                let meeple_img = image_bytes_to_image(meeple_bytes);

                let (sx, sy) = match seg as u8 {
                    0 => (8, 0),
                    1 => (24, 0),
                    2 => (40, 0),
                    3 => (48, 8),
                    4 => (48, 24),
                    5 => (48, 40),
                    6 => (40, 48),
                    7 => (24, 48),
                    8 => (8, 48),
                    9 => (0, 40),
                    10 => (0, 24),
                    11 => (0, 8),
                    _ => unreachable!(),
                };

                overlay(&mut tile_img, &meeple_img, sx, sy);
            }

            // draw cloister meeples
            if let Some(player) = self.get_meeple_on_cloister(*coord) {
                let meeple_bytes = player.meeple_image_bytes();
                let meeple_img = image_bytes_to_image(meeple_bytes);

                overlay(&mut tile_img, &meeple_img, 24, 24);
            }

            // draw the tile
            overlay(&mut buf, &tile_img, j * 64, i * 64);
        }

        if let Some(dir) = path.as_ref().parent() {
            create_dir_all(dir).unwrap();
        }
        buf.save(path).unwrap();
    }

    /// Perform a general floodfill to get information about a connected feature area.
    /// As a side-effect, `occupied` flags are also set during the search, so be careful **not** to
    /// run this on features where no meeple has been placed yet.
    /// See [`FloodfillConfig`] and [`FloodfillOutput`].
    pub fn floodfill(&mut self, config: FloodfillConfig) -> FloodfillOutput {
        let FloodfillConfig {
            from_coord,
            from_feature_id,
            should_set_occupied,
        } = config;

        let mut visited = HashSet::<(Coord, FeatureId)>::new();
        let mut stack = Vec::<(Coord, FeatureId)>::new();

        let mut pennant_count = 0;
        let mut has_open_edge = None;

        stack.push((from_coord, from_feature_id));
        visited.insert((from_coord, from_feature_id));

        while let Some((center_coord, center_feature_id)) = stack.pop() {
            let center = self.entry_at_mut(center_coord).unwrap();
            if should_set_occupied {
                // set occupied state
                center.set_occupied(center_feature_id);
            }

            // drop mutable reference
            let center = self.entry_at(center_coord).unwrap();

            // update pennant count
            if center.tile.has_pennant_on(center_feature_id) {
                pennant_count += 1;
            }

            for seg in Segment::iter() {
                if center.tile.feature_id_of(seg) != center_feature_id {
                    // not the feature id we're processing this time
                    continue;
                }

                let neighbor_coord = center_coord.neighbor_on(seg.side());
                if let Some(neighbor) = self.entry_at(neighbor_coord) {
                    let neighbor_feature_id = neighbor.tile.feature_id_of(seg.opposite());

                    // push neighbor feature onto the stack if it's not visited
                    if visited.insert((neighbor_coord, neighbor_feature_id)) {
                        stack.push((neighbor_coord, neighbor_feature_id));
                    }

                    // this is a closed edge, lazy-initialize the open edge flag
                    has_open_edge = has_open_edge.or(Some(false));
                } else {
                    // this is an open edge, raise the open edge flag
                    has_open_edge = Some(true);
                }
            }
        }

        // convert has_open_edge to is_closed
        let is_closed = match has_open_edge {
            // un-initialized; has open edge
            None => false,
            // initialized and not yet raised; no open edge
            Some(false) => true,
            // initialized and raised; has open edge
            Some(true) => false,
        };

        FloodfillOutput {
            visited,
            pennant_count,
            is_closed,
        }
    }

    fn field_floodfill(&self, config: FieldFloodfillConfig) -> FieldFloodfillOutput {
        let FieldFloodfillConfig {
            from_coord,
            from_feature_id,
            city_ids,
        } = config;

        let mut visited = HashSet::<(Coord, FeatureId)>::new();
        let mut stack = Vec::<(Coord, FeatureId)>::new();
        let mut adjacent_cities = HashSet::<usize>::new();

        stack.push((from_coord, from_feature_id));
        visited.insert((from_coord, from_feature_id));

        while let Some((center_coord, center_feature_id)) = stack.pop() {
            let center = self.entry_at(center_coord).unwrap();
            for seg in Segment::iter() {
                if center.tile.feature_id_of(seg) != center_feature_id {
                    continue;
                }

                // count cities
                for city_feature_id in center.tile.adjacent_city_feature_ids(center_feature_id) {
                    guard!(let Some(&city_id) = city_ids.get(&(center_coord, city_feature_id)) else { continue; });
                    adjacent_cities.insert(city_id);
                }

                // recurse
                let neighbor_coord = center_coord.neighbor_on(seg.side());
                if let Some(neighbor) = self.entry_at(neighbor_coord) {
                    let neighbor_feature_id = neighbor.tile.feature_id_of(seg.opposite());

                    // push neighbor feature onto the stack if it's not visited
                    if visited.insert((neighbor_coord, neighbor_feature_id)) {
                        stack.push((neighbor_coord, neighbor_feature_id));
                    }
                }
            }
        }

        FieldFloodfillOutput {
            visited,
            city_count: adjacent_cities.len(),
        }
    }

    /// Update scores based on a feature area information, and optionally recycle meeples for
    /// in-game scoring.
    fn update_scores(&mut self, config: UpdateScoreConfig) -> (u8, &'static [Player]) {
        let UpdateScoreConfig {
            visited,
            pennant_count,
            score_multiplier,
            should_recycle_meeples,
        } = config;

        // count distrinct coordinates in `coord_visited`
        let mut coord_visited = visited
            .iter()
            .map(|(coord, _feature_id)| coord)
            .sorted()
            .collect_vec();
        coord_visited.dedup();
        let tile_count = coord_visited.len();

        // recycle (optional) & count meeples
        let mut meeple_counts = [0, 0];
        for &(coord, feature_id) in visited.iter() {
            // recycle or not, depending on the flag
            let maybe_meeple = if should_recycle_meeples {
                self.meeples
                    .remove(&MeepleCoord::Edge(coord, feature_id as u8))
            } else {
                self.meeples
                    .get(&MeepleCoord::Edge(coord, feature_id as u8))
                    .cloned()
            };

            if let Some(player) = maybe_meeple {
                meeple_counts[player.i()] += 1;
                if should_recycle_meeples {
                    self.meeples_remain[player.i()] += 1;
                }
            }
        }

        let area_score = (tile_count + pennant_count) * score_multiplier;
        let area_score = area_score as u8;

        let scored_players = self.add_area_score(meeple_counts, area_score);

        (area_score, scored_players)
    }

    fn field_update_scores(&mut self, config: FieldUpdateScoreConfig) -> (u8, &'static [Player]) {
        let FieldUpdateScoreConfig {
            visited,
            city_count,
        } = config;

        // count meeples
        let mut meeple_counts = [0, 0];
        for &(coord, feature_id) in visited.iter() {
            if let Some(&player) = self
                .meeples
                .get(&MeepleCoord::Edge(coord, feature_id as u8))
            {
                meeple_counts[player.i()] += 1;
            }
        }

        let area_score = (city_count * 3) as u8;

        let scored_players = self.add_area_score(meeple_counts, area_score);

        (area_score, scored_players)
    }

    fn endgame_score_update(&mut self) {
        // { (coordinates, feature id) }
        let mut area_visited = HashSet::<(Coord, FeatureId)>::new();
        // (coordinates, feature id) -> city id
        let mut city_ids = HashMap::<(Coord, FeatureId), usize>::new();
        let mut next_city_id = 0;

        let tiles = self.tiles.clone();

        // score incomplete cities & roads
        for (&coord, &entry) in &tiles {
            for feature_id in entry.tile.kind.feature_ids() {
                let feature_kind = entry.tile.feature_kind_on(feature_id);

                if matches!(feature_kind, Feature::City | Feature::Road) == false {
                    continue;
                }

                let has_already_visited = area_visited.contains(&(coord, feature_id));
                if has_already_visited {
                    continue;
                }

                let floodfill_output = self.floodfill(FloodfillConfig {
                    should_set_occupied: false,
                    from_coord: coord,
                    from_feature_id: feature_id,
                });

                // store visited information
                for &(visited_coord, visited_feature_id) in floodfill_output.visited.iter() {
                    area_visited.insert((visited_coord, visited_feature_id));
                }

                // store *completed* city information
                if feature_kind == Feature::City && floodfill_output.is_closed {
                    for &(visited_coord, visited_feature_id) in floodfill_output.visited.iter() {
                        city_ids.insert((visited_coord, visited_feature_id), next_city_id);
                    }
                    next_city_id += 1;
                }

                // score the area if it's incomplete
                if floodfill_output.is_closed == false {
                    let FloodfillOutput {
                        ref visited,
                        pennant_count,
                        ..
                    } = floodfill_output;

                    let (score, scored_players) = self.update_scores(UpdateScoreConfig {
                        visited,
                        pennant_count,
                        should_recycle_meeples: false,
                        score_multiplier: 1,
                    });
                    if score > 0 {
                        debug!(
                            "{:?} at {:?}, {:?} scored {} for {:?}",
                            feature_kind, coord, feature_id, score, scored_players
                        );
                        debug!("Area: {:?}", visited);
                        if feature_kind == Feature::City {
                            debug!("pennant_count: {}", pennant_count);
                        }
                    }
                }
            }
        }

        // score fields
        for (&coord, &entry) in &tiles {
            for feature_id in entry.tile.kind.feature_ids() {
                let kind = entry.tile.feature_kind_on(feature_id);
                if kind != Feature::Field {
                    continue;
                }

                let has_already_visited = area_visited.contains(&(coord, feature_id));
                if has_already_visited {
                    continue;
                }

                let floodfill_output = self.field_floodfill(FieldFloodfillConfig {
                    from_coord: coord,
                    from_feature_id: feature_id,
                    city_ids: &city_ids,
                });

                // store visited information
                for &(visited_coord, visited_feature_id) in floodfill_output.visited.iter() {
                    area_visited.insert((visited_coord, visited_feature_id));
                }

                // score the field if city count is non-zero
                if floodfill_output.city_count != 0 {
                    let FieldFloodfillOutput {
                        ref visited,
                        city_count,
                    } = floodfill_output;

                    let (score, scored_players) =
                        self.field_update_scores(FieldUpdateScoreConfig {
                            visited,
                            city_count,
                        });
                    if score > 0 {
                        debug!(
                            "{:?} at {:?}, {:?} scored {} for {:?}",
                            kind, coord, feature_id, score, scored_players
                        );
                    }
                }
            }
        }

        // score cloisters
        for (&meeple_coord, &player) in &self.meeples {
            guard!(let MeepleCoord::Cloister(coord) = meeple_coord else { continue; });

            let around_count = self.count_tiles_around(coord);
            let score = around_count as u8 + 1;
            self.scores[player.i()] += score;

            debug!("Cloister at {:?} scored {} for {:?}", coord, score, player);
        }
    }

    fn has_occupied_neighbor(&self, coord: Coord, feature_id: FeatureId) -> bool {
        let tile = self
            .tile_at(coord)
            .expect("has_occupied_neighbor called with empty tile slot");
        Segment::iter().filter(|seg| {
            // leave only segments of this feature_id
            tile.feature_id_of(*seg) == feature_id
        }).any(|seg| {
            guard!(let Some(neighbor) = self.entry_at(coord.neighbor_on(seg.side())) else { return false });
            neighbor.occupied_on_seg(seg.opposite())
        })
    }

    /// Update a fixed amount of score (`area_score`) to the players, depending on the number of
    /// their meeples.
    fn add_area_score(&mut self, meeple_counts: [usize; 2], area_score: u8) -> &'static [Player] {
        use Player::*;
        let red_count = meeple_counts[Red.i()];
        let blue_count = meeple_counts[Blue.i()];
        let scored_players: &[Player] = match red_count.cmp(&blue_count) {
            Ordering::Greater => &[Red],
            Ordering::Less => &[Blue],
            Ordering::Equal if red_count != 0 => &[Red, Blue],
            _ => return &[],
        };

        for player in scored_players {
            self.scores[player.i()] += area_score;
        }

        scored_players
    }

    // iteration api
    pub fn tiles_iter(&self) -> impl Iterator<Item = (Coord, Tile)> + '_ {
        self.tiles
            .iter()
            .map(|(&coord, &entry)| (coord, entry.tile))
    }

    pub fn meeples_iter(&self) -> impl Iterator<Item = (MeepleCoord, Player)> + '_ {
        self.meeples
            .iter()
            .map(|(&meeple_coord, &player)| (meeple_coord, player))
    }

    pub fn deck_iter(&self) -> impl Iterator<Item = TileKind> + '_ {
        self.deck.iter().cloned()
    }

    pub fn turn(&self) -> Player {
        self.turn
    }

    pub fn remain(&self) -> usize {
        self.deck.len()
    }
}

#[derive(Debug)]
pub struct FloodfillConfig {
    pub from_coord: Coord,
    pub from_feature_id: FeatureId,
    pub should_set_occupied: bool,
}

#[derive(Debug)]
pub struct FloodfillOutput {
    /// Tile-features on the board that are connected to `from_coord`
    pub visited: HashSet<(Coord, FeatureId)>,
    pub pennant_count: usize,
    pub is_closed: bool,
}

#[derive(Debug)]
struct UpdateScoreConfig<'a> {
    visited: &'a HashSet<(Coord, FeatureId)>,
    pennant_count: usize,
    score_multiplier: usize,
    should_recycle_meeples: bool,
}

#[derive(Debug)]
struct FieldFloodfillConfig<'a> {
    from_coord: Coord,
    from_feature_id: FeatureId,
    /// maps: (coordinates, feature id) -> city id
    city_ids: &'a HashMap<(Coord, FeatureId), usize>,
}

#[derive(Debug)]
struct FieldFloodfillOutput {
    visited: HashSet<(Coord, FeatureId)>,
    city_count: usize,
}

#[derive(Debug)]
struct FieldUpdateScoreConfig<'a> {
    visited: &'a HashSet<(Coord, FeatureId)>,
    city_count: usize,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Action {
    pub coord: Coord,
    pub rotation: Rotation,
    pub meeple_action: MeepleAction,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub enum MeepleAction {
    Edge(u8),
    Cloister,
    None,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Serialize, Deserialize)]
pub struct Coord {
    pub i: i8,
    pub j: i8,
}

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy, Serialize, Deserialize)]
pub enum MeepleCoord {
    Edge(Coord, u8),
    Cloister(Coord),
}

#[repr(u8)]
#[derive(Debug, PartialEq, Eq, Clone, Copy, EnumIter, IntoPrimitive)]
pub enum Side {
    North = 0,
    East = 1,
    South = 2,
    West = 3,
}

impl Side {
    fn opposite(&self) -> Self {
        match *self {
            Side::North => Side::South,
            Side::East => Side::West,
            Side::South => Side::North,
            Side::West => Side::East,
        }
    }
}

impl Coord {
    pub fn neighbors(&self) -> [(Side, Coord); 4] {
        array_init::from_iter(Side::iter().map(|side| (side, self.neighbor_on(side)))).unwrap()
    }

    fn eight_adjacents(&self) -> [Coord; 8] {
        let Coord { i, j } = *self;
        [
            crd!(i - 1, j - 1),
            crd!(i - 1, j),
            crd!(i - 1, j + 1),
            crd!(i, j - 1),
            crd!(i, j + 1),
            crd!(i + 1, j - 1),
            crd!(i + 1, j),
            crd!(i + 1, j + 1),
        ]
    }

    pub fn neighbor_on(&self, side: Side) -> Coord {
        let Coord { i, j } = *self;
        match side {
            Side::North => crd!(i - 1, j),
            Side::East => crd!(i, j + 1),
            Side::South => crd!(i + 1, j),
            Side::West => crd!(i, j - 1),
        }
    }
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize)]
pub struct Tile {
    pub kind: TileKind,
    pub rotation: Rotation,
}

impl Tile {
    pub fn features(&self) -> impl Iterator<Item = (FeatureId, Feature)> + '_ {
        (0..self.kind.num_features() as u8).map(move |i| {
            let feature_id = FeatureId::from_u8(i).unwrap();
            let feature_kind = self.feature_kind_on(feature_id);
            (feature_id, feature_kind)
        })
    }

    /// Get features on a side, ordered in top-down / left-right way as [`SideFeatures`]
    fn side_features(&self, side: Side) -> SideFeatures {
        let Tile { kind, rotation } = *self;
        cast_all!(kind, rotation);

        let edge_features = &EDGE_FEATURES[kind][rotation];
        // top-down, left-right indicies
        let [x, y, z] = match side {
            Side::North => [0, 1, 2],
            Side::East => [3, 4, 5],
            Side::South => [8, 7, 6],
            Side::West => [11, 10, 9],
        };
        let data: [Feature; 3] = [edge_features[x], edge_features[y], edge_features[z]];
        SideFeatures { data }
    }

    fn image(&self) -> RgbaImage {
        let img_bytes = TILE_IMAGE_BYTES[self.kind.i()];
        let tile_img = image_bytes_to_image(img_bytes);

        // image crate's rotation is clockwise, while our rotation is anticlockwise
        match self.rotation {
            Rotation::R0 => tile_img,
            Rotation::R90 => rotate270(&tile_img),
            Rotation::R180 => rotate180(&tile_img),
            Rotation::R270 => rotate90(&tile_img),
        }
    }

    pub fn feature_id_of(&self, seg: Segment) -> FeatureId {
        cast_all!(kind = self.kind, rotation = self.rotation, seg);
        FeatureId::from_u8(FEATURE_IDS[kind][rotation][seg]).unwrap()
    }

    fn feature_kind_on(&self, feature_id: FeatureId) -> Feature {
        cast_all!(kind = self.kind, feature_id);
        FEATURE_KINDS[kind][feature_id]
    }

    fn has_pennant_on(&self, feature_id: FeatureId) -> bool {
        cast_all!(kind = self.kind, feature_id);
        HAS_PENNANT[kind][feature_id]
    }

    pub fn has_cloister(&self) -> bool {
        cast_all!(kind = self.kind);
        HAS_CLOISTER[kind]
    }

    /// feature id (of field) -> [feature id (of cities)]
    pub fn adjacent_city_feature_ids(
        &self,
        feature_id: FeatureId,
    ) -> impl Iterator<Item = FeatureId> {
        cast_all!(kind = self.kind, feature_id);
        let num = FIELD_FEATURE_NUM_ADJ_CITIES[kind][feature_id];
        FIELD_FEATURE_ADJ_CITIES[kind][feature_id][..num]
            .iter()
            .cloned()
            .map(FeatureId::from_u8)
            .map(Option::unwrap)
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, EnumIter, FromPrimitive, IntoPrimitive)]
pub enum Segment {
    S0 = 0,
    S1 = 1,
    S2 = 2,
    S3 = 3,
    S4 = 4,
    S5 = 5,
    S6 = 6,
    S7 = 7,
    S8 = 8,
    S9 = 9,
    S10 = 10,
    S11 = 11,
}

impl Segment {
    pub fn side(&self) -> Side {
        const SIDE_TABLE: [Side; 12] = [
            Side::North,
            Side::North,
            Side::North,
            Side::East,
            Side::East,
            Side::East,
            Side::South,
            Side::South,
            Side::South,
            Side::West,
            Side::West,
            Side::West,
        ];

        // segment's value never exceeds the length of SIDE_TABLE
        SIDE_TABLE[self.i()]
    }

    pub fn opposite(&self) -> Self {
        const SEG_OPPOSITE_TABLE: [u8; 12] = [8, 7, 6, 11, 10, 9, 2, 1, 0, 5, 4, 3];

        // segment's value never exceeds the length of SEG_OPPOSITE_TABLE
        Segment::from_u8(SEG_OPPOSITE_TABLE[self.i()]).unwrap()
    }
}

#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash, EnumIter, FromPrimitive, IntoPrimitive)]
pub enum FeatureId {
    F0 = 0,
    F1 = 1,
    F2 = 2,
    F3 = 3,
    F4 = 4,
    F5 = 5,
    F6 = 6,
    F7 = 7,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct SideFeatures {
    /// The features, ordered in top-down, left-right way
    data: [Feature; 3],
}

/// Anticlockwise rotations
#[repr(u8)]
#[derive(
    Debug,
    Clone,
    Copy,
    PartialEq,
    Eq,
    Hash,
    EnumIter,
    FromPrimitive,
    IntoPrimitive,
    Serialize,
    Deserialize,
    PartialOrd,
    Ord,
)]
pub enum Rotation {
    R0 = 0,
    R90 = 1,
    R180 = 2,
    R270 = 3,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Feature {
    City,
    Field,
    Road,
    Cloister,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, EnumIter, IntoPrimitive, Serialize, Deserialize)]
#[repr(u8)]
pub enum Player {
    Red = 0,
    Blue = 1,
}

impl Player {
    pub fn opposite(&self) -> Self {
        match *self {
            Self::Red => Self::Blue,
            Self::Blue => Self::Red,
        }
    }

    fn meeple_image_bytes(&self) -> &'static [u8] {
        match *self {
            Player::Red => include_bytes!("../../resources/misc/MeepleRed.png"),
            Player::Blue => include_bytes!("../../resources/misc/MeepleBlue.png"),
        }
    }
}

trait EnumChoose {
    /// Choose enum variant randomly
    fn choose() -> Self;
}
impl<T> EnumChoose for T
where
    T: IntoEnumIterator,
{
    fn choose() -> Self {
        Self::iter().choose(&mut thread_rng()).unwrap()
    }
}

trait AsIndex {
    fn i(&self) -> usize;
}

impl<T> AsIndex for T
where
    T: Into<u8> + Copy,
{
    fn i(&self) -> usize {
        Into::<u8>::into(*self) as usize
    }
}

fn image_bytes_to_image(bytes: &[u8]) -> RgbaImage {
    guard!(let image::DynamicImage::ImageRgba8(rgba_img) = image::io::Reader::with_format(
        Cursor::new(bytes),
        image::ImageFormat::Png,
    )
        .decode()
        .unwrap() else { panic!("bad resource image variant") });
    rgba_img
}
