use ccs_gnn::convert::graph_from_board;
use rand::prelude::*;

fn main() {
    for _ in 0..32 {
        let mut board = ccs::Board::new();
        while board.is_ended() == false {
            board.draw();
            let action = *board.legal_actions().choose(&mut thread_rng()).unwrap();
            let graph_output = graph_from_board(board.clone());
            board.do_action(action);
            println!("{:?}", graph_output);
        }
    }
}
