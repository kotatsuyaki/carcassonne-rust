Carcassonne Rust
================

See the blog posts [part I](https://akitaki.gitlab.io/carcassonne-ai/) and [part II](https://akitaki.gitlab.io/carcassonne-ai-part-2/)
for more information.

This repository contains several projects related to the board game
[Carcassonne](https://en.wikipedia.org/wiki/Carcassonne_(board_game)).

- `./carcassonne/`: Base game logic.
- `./carcassonne-mcts/`: A fast implementation of MCTS algorithm for Carcassonne.
- `./carcassonne-gnn/`: A deep learning agent for Carcassonne inspired by [ScalableAlphaZero](https://arxiv.org/abs/2107.08387).
- `./server/`: The backend server for [Carcassonne UI](https://gitlab.com/Akitaki/carcassonne-ui),
  a minimal gameplay interface for debug purposes.
