use std::{
    convert::TryInto,
    sync::Arc,
    time::{Duration, Instant},
};

use anyhow::Result;
use ccs_mcts::Mcts;
use hashbrown::HashMap;
use itertools::Itertools;
use rand::prelude::*;
use tokio::sync::{watch, RwLock};
use tonic::{transport::Server, Request, Response, Status};
use tracing::{debug, info, warn};
use uuid::Uuid;

use proto::*;

#[tokio::main(flavor = "multi_thread", worker_threads = 16)]
async fn main() -> Result<()> {
    eprintln!("Environment variable options: `GRPCPORT`, `MCTSITERS`");

    let _guard = init_tracing()?;

    let port = std::env::var("GRPCPORT").unwrap_or("50051".to_string());
    info!("Using port {}", port);
    let addr = format!("0.0.0.0:{port}", port = port).parse()?;
    let game_service = GameService::new();
    game_service.start_periodic_cleanup().await;

    Server::builder()
        .accept_http1(true)
        .add_service(tonic_web::enable(
            game_service_server::GameServiceServer::new(game_service),
        ))
        .serve(addr)
        .await?;

    Ok(())
}

#[derive(Debug)]
struct GameService {
    entries: Arc<RwLock<HashMap<u32, GameEntry>>>,
}

#[derive(Debug, Clone)]
struct GameEntry {
    inner: Arc<RwLock<GameEntryInner>>,
    with_npc: bool,
    rx: watch::Receiver<u32>,
}

#[derive(Debug)]
struct GameEntryInner {
    state: GameState,
    tx: watch::Sender<u32>,
}

#[derive(Clone, Debug)]
struct GameState {
    serial: u32,
    board: Option<ccs::Board>,
    legal_actions: Option<Vec<ccs::Action>>,
    /// Map from token to participant
    players: HashMap<Uuid, Participant>,
    touched: Instant,
}

#[derive(Clone, Debug)]
struct Participant {
    role: ccs::Player,
    name: String,
}

#[derive(Clone, Debug)]
struct GameStateOutput {
    state: GameState,
    rx: watch::Receiver<u32>,
    serial: u32,
}

#[tonic::async_trait]
impl game_service_server::GameService for GameService {
    async fn create_room(
        &self,
        request: Request<CreateRoomRequest>,
    ) -> Result<Response<CreateRoomResponse>, Status> {
        let addr = request.remote_addr();

        let CreateRoomRequest { with_npc } = request.into_inner();

        // choose a room id
        let entries = self.entries.read().await;
        if entries.len() == 99999 - 10000 + 1 {
            return Err(Status::resource_exhausted(
                "All available room IDs are occupied at the moment",
            ));
        }
        let room_id = loop {
            let room_id = thread_rng().gen_range(10000..=99999);
            if entries.contains_key(&room_id) == false {
                break room_id;
            }
        };
        drop(entries);

        let mut entries = self.entries.write().await;
        entries.insert(room_id, GameEntry::new(with_npc));
        drop(entries);

        info!(?addr, room_id, "created room");

        Ok(Response::new(CreateRoomResponse { room_id }))
    }

    async fn join_room(
        &self,
        request: Request<JoinRoomRequest>,
    ) -> Result<Response<JoinRoomResponse>, Status> {
        let addr = request.remote_addr();

        let JoinRoomRequest { room_id, name } = request.into_inner();

        let mut entry = self.entry_for_room(room_id).await?;
        let (player, token) = entry.join_room(&name).await?;

        info!(?addr, room_id, name = name.as_str(), ?player, "joined room");

        Ok(Response::new(JoinRoomResponse {
            me: Player::from(player) as i32,
            token: token.to_string(),
        }))
    }

    async fn game_state(
        &self,
        request: Request<GameStateRequest>,
    ) -> Result<Response<GameStateResponse>, Status> {
        let addr = request.remote_addr();

        let GameStateRequest {
            room_id,
            last_serial,
        } = request.into_inner();

        let entry = self.entry_for_room(room_id).await?;
        let mut state_output = entry.game_state().await?;

        let state = if last_serial < state_output.serial {
            state_output.state
        } else {
            // serial not new enough, wait for one state update
            state_output.wait_for_update().await?;

            let entry = self.entry_for_room(room_id).await?;
            entry.game_state().await?.state
        };

        // convert board if is some
        let board = state.board.clone().map(|board| board.into());

        // convert legal actions is is some
        let legal_actions = if let Some(legal_actions) = state.legal_actions {
            legal_actions
                .into_iter()
                .map(|action| Action::from(action))
                .collect_vec()
        } else {
            vec![]
        };

        // convert player names
        let mut player_names = vec![String::new(), String::new()];
        for (_token, participant) in state.players.iter() {
            let index = participant.role as u8 as usize;
            player_names[index] = participant.name.clone();
        }

        debug!(?addr, room_id, "requested game state");

        Ok(Response::new(GameStateResponse {
            serial: state.serial,
            started: state.players.len() >= 2 || (entry.with_npc && state.players.len() >= 1),
            num_players: state.players.len() as u32,
            board,
            legal_actions,
            player_names,
        }))
    }

    async fn do_action(
        &self,
        request: Request<DoActionRequest>,
    ) -> Result<Response<Empty>, Status> {
        let addr = request.remote_addr();

        let DoActionRequest {
            room_id,
            action,
            token,
        } = request.into_inner();
        let token = Uuid::parse_str(&token)
            .map_err(|_| Status::invalid_argument("Failed to parse token"))?;
        guard::guard!(let Some(action) = action else {
           return Err(Status::invalid_argument("Missing action parameter"));
        });
        let action = action
            .try_into()
            .map_err(|_| Status::invalid_argument("Failed to convert action"))?;
        let entry = self.entry_for_room(room_id).await?;
        entry.do_action(action, token).await?;

        if entry.with_npc {
            tokio::spawn(async move {
                if let Err(e) = entry.do_npc_action().await {
                    warn!("Error during npc action: {}", e);
                }
            });
        }

        info!(?addr, room_id, ?action, "did action");

        Ok(Response::new(Empty {}))
    }
}

impl GameService {
    fn new() -> Self {
        let service = GameService {
            entries: Arc::new(RwLock::new(HashMap::new())),
        };
        service
    }

    async fn entry_for_room(&self, room_id: u32) -> Result<GameEntry, Status> {
        let entries = self.entries.read().await;
        match entries.get(&room_id) {
            Some(entry) => Ok(entry.clone()),
            None => Err(Status::not_found("Room not found (note that rooms will be removed if they've been inactive for a long period of time)")),
        }
    }

    async fn start_periodic_cleanup(&self) {
        // cleanup rooms that are inactive for 120 minutes
        const CLEANUP_THRESHOLD: Duration = Duration::from_secs(60 * 120);
        const CLEANUP_ITNERVAL: tokio::time::Duration = tokio::time::Duration::from_secs(60);

        let entries = self.entries.clone();
        tokio::spawn(async move {
            let mut interval = tokio::time::interval(CLEANUP_ITNERVAL);
            loop {
                interval.tick().await;
                let mut entries = entries.write().await;

                // room ids that are going to be cleaned up
                let mut room_ids = vec![];
                for (room_id, entry) in entries.iter_mut() {
                    let touched = entry.inner.read().await.state.touched;
                    let elapsed = Instant::now() - touched;
                    if elapsed >= CLEANUP_THRESHOLD {
                        room_ids.push(*room_id);
                    }
                }

                if room_ids.is_empty() == false {
                    info!("Cleaning up rooms: {:?}", room_ids);
                }
                for room_id in room_ids {
                    entries.remove(&room_id);
                }
            }
        });
    }
}

impl GameEntry {
    fn new(with_npc: bool) -> Self {
        let (tx, rx) = watch::channel(1);
        GameEntry {
            with_npc,
            inner: Arc::new(RwLock::new(GameEntryInner {
                state: GameState {
                    serial: 1,
                    board: None,
                    legal_actions: None,
                    players: HashMap::new(),
                    touched: Instant::now(),
                },
                tx,
            })),
            rx,
        }
    }

    /// Returns the player and associated token
    async fn join_room(&mut self, name: impl AsRef<str>) -> Result<(ccs::Player, Uuid), Status> {
        let mut inner = self.inner.write().await;

        // Decide the color of the new player
        let player = if inner.state.players.is_empty() {
            ccs::Player::Red
        } else if inner.state.players.len() == 1 {
            ccs::Player::Blue
        } else {
            return Err(Status::resource_exhausted("Room is full"));
        };

        // Initialize state if we have enough players
        let num_players_before = inner.state.players.len();
        if num_players_before == 1 || (self.with_npc && num_players_before == 0) {
            let mut board = ccs::Board::new();

            board.draw();
            let legal_actions = board.legal_actions();

            inner.state.board = Some(board);
            inner.state.legal_actions = Some(legal_actions);
        }

        let token = Uuid::new_v4();
        inner.state.players.insert(
            token,
            Participant {
                role: player,
                name: name.as_ref().to_string(),
            },
        );

        // have pushed something, increase serial
        inner.state.serial += 1;
        let serial = inner.state.serial;
        inner
            .tx
            .send(serial)
            .map_err(|_| Status::internal("Failed to send serial"))?;

        Ok((player, token))
    }

    async fn game_state(&self) -> Result<GameStateOutput, Status> {
        let mut rx = self.rx.clone();
        let serial = *rx.borrow_and_update();
        let state = self.inner.read().await.state.clone();

        Ok(GameStateOutput { state, rx, serial })
    }

    async fn do_action(&self, action: ccs::Action, token: Uuid) -> Result<(), Status> {
        let mut inner = self.inner.write().await;
        let GameState {
            ref mut legal_actions,
            ref mut board,
            ref mut serial,
            ref players,
            ..
        } = inner.state;
        guard::guard!(let Some(ref mut legal_actions) = legal_actions else {
            return Err(Status::failed_precondition("No actions available"));
        });
        guard::guard!(let Some(ref mut board) = board else {
            return Err(Status::failed_precondition("No board available"));
        });
        match players.get(&token) {
            // Has this participant but not his/her turn
            Some(participant) if participant.role != board.turn() => {
                return Err(Status::failed_precondition("Not your turn"));
            }
            // No this participant at all
            None => {
                return Err(Status::unauthenticated("Invalid token"));
            }
            _ => {}
        }

        if legal_actions.contains(&action) {
            board.do_action(action);
            if board.is_ended() == false {
                board.draw();
                *legal_actions = board.legal_actions();
            }
        } else {
            warn!("illegal action: {:?}", action);
            return Err(Status::failed_precondition("Action is illegal"));
        }

        *serial += 1;
        let serial = serial.clone();
        inner
            .tx
            .send(serial)
            .map_err(|_| Status::internal("Failed to send serial"))?;
        inner.state.touched = Instant::now();

        Ok(())
    }

    /// Performs npc action.
    ///
    /// Does nothing if the game is already ended.
    /// Returns error if
    /// * No actions available.
    /// * No board available.
    /// * Serial sending fails.
    async fn do_npc_action(&self) -> Result<(), Status> {
        // clone the board for manipulation
        let mut temp_board = {
            let mut inner = self.inner.write().await;
            let GameState { ref mut board, .. } = inner.state;
            guard::guard!(let Some(ref mut board) = board else {
                return Err(Status::failed_precondition("No board available"));
            });
            if board.is_ended() {
                return Ok(());
            }
            board.clone()
        };

        // solve and update state
        let iters = std::env::var("MCTSITERS")
            .unwrap_or("500".to_string())
            .parse()
            .unwrap();
        let action = Mcts::new().mcts_loop(temp_board.clone(), iters);

        temp_board.do_action(action);

        // write state
        {
            let mut inner = self.inner.write().await;
            let GameState {
                ref mut legal_actions,
                ref mut board,
                ref mut serial,
                ..
            } = inner.state;
            guard::guard!(let Some(ref mut legal_actions) = legal_actions else {
                return Err(Status::failed_precondition("No actions available"));
            });
            guard::guard!(let Some(ref mut board) = board else {
                return Err(Status::failed_precondition("No board available"));
            });

            *board = temp_board;
            if board.is_ended() == false {
                board.draw();
                *legal_actions = board.legal_actions();
            }

            *serial += 1;
            let serial = serial.clone();
            inner
                .tx
                .send(serial)
                .map_err(|_| Status::internal("Failed to send serial"))?;
            inner.state.touched = Instant::now();
        }

        Ok(())
    }
}

impl GameStateOutput {
    async fn wait_for_update(&mut self) -> Result<(), Status> {
        self.rx
            .changed()
            .await
            .map_err(|_| Status::internal("Failed to listen to change"))?;
        Ok(())
    }
}

mod proto {
    use std::convert::{TryFrom, TryInto};

    use hashbrown::HashMap;
    use num_traits::FromPrimitive;

    tonic::include_proto!("carcassonne");

    impl From<ccs::Board> for Board {
        fn from(board: ccs::Board) -> Self {
            // prepare meeples
            let mut meeples_by_coord = HashMap::new();
            for (meeple_coord, player) in board.meeples_iter() {
                let (coord, meeple) = convert_meeple((meeple_coord, player));
                meeples_by_coord.insert(coord, meeple);
            }

            // convert tiles
            let mut tiles = vec![];
            for (coord, tile) in board.tiles_iter() {
                tiles.push(TileEntry {
                    tile: Some(Tile::from(tile)),
                    coord: Some(Coord::from(coord)),
                    meeple: meeples_by_coord.get(&coord).cloned(),
                });
            }

            // convert deck
            let mut deck = vec![];
            for kind in board.deck_iter() {
                let kind = TileKind::from(kind) as i32;
                deck.push(kind);
            }

            // convert turn
            let turn = Player::from(board.turn()) as i32;

            // convert scores
            let scores = board.scores().iter().map(|&i| i as u32).collect();

            // convert meeples
            let remaining_meeples = board.meeples_remain().iter().map(|&i| i as u32).collect();

            Board {
                tiles,
                deck,
                turn,
                scores,
                remaining_meeples,
            }
        }
    }

    impl From<ccs::Tile> for Tile {
        fn from(tile: ccs::Tile) -> Self {
            Tile {
                kind: TileKind::from(tile.kind) as i32,
                rotation: Rotation::from(tile.rotation) as i32,
                meeple_on_feature: None,
                meeple_on_cloister: None,
            }
        }
    }

    impl From<ccs::Player> for Player {
        fn from(player: ccs::Player) -> Self {
            match player {
                ccs::Player::Red => Self::Red,
                ccs::Player::Blue => Self::Blue,
            }
        }
    }

    impl From<ccs::Coord> for Coord {
        fn from(coord: ccs::Coord) -> Self {
            Coord {
                i: coord.i as i32,
                j: coord.j as i32,
            }
        }
    }

    impl From<ccs::TileKind> for TileKind {
        fn from(kind: ccs::TileKind) -> Self {
            TileKind::from_i32(kind as u8 as i32).unwrap()
        }
    }

    impl From<ccs::Rotation> for Rotation {
        fn from(rotation: ccs::Rotation) -> Self {
            Rotation::from_i32(rotation as u8 as i32).unwrap()
        }
    }

    impl From<ccs::Action> for Action {
        fn from(action: ccs::Action) -> Self {
            Action {
                rotation: Rotation::from(action.rotation) as i32,
                coord: Some(Coord::from(action.coord)),
                meeple_action: Some(MeepleAction::from(action.meeple_action)),
            }
        }
    }

    impl From<ccs::MeepleAction> for MeepleAction {
        fn from(meeple_action: ccs::MeepleAction) -> Self {
            let converted = match meeple_action {
                ccs::MeepleAction::None => meeple_action::MeepleAction::None(true),
                ccs::MeepleAction::Edge(value) => meeple_action::MeepleAction::Edge(value as u32),
                ccs::MeepleAction::Cloister => meeple_action::MeepleAction::Cloister(true),
            };
            MeepleAction {
                meeple_action: Some(converted),
            }
        }
    }

    /// Convert from library tuple to api tuple representation of a meeple.
    ///
    /// This isn't done using the [From] trait, because tuples are considered to be foreign.
    fn convert_meeple(
        (meeple_coord, player): (ccs::MeepleCoord, ccs::Player),
    ) -> (ccs::Coord, Meeple) {
        let (coord, meeple_action) = match meeple_coord {
            ccs::MeepleCoord::Edge(coord, feature_id) => (
                coord,
                MeepleAction {
                    meeple_action: Some(meeple_action::MeepleAction::Edge(feature_id as u32)),
                },
            ),
            ccs::MeepleCoord::Cloister(coord) => (
                coord,
                MeepleAction {
                    meeple_action: Some(meeple_action::MeepleAction::Cloister(true)),
                },
            ),
        };
        let player = Player::from(player) as i32;

        (
            coord,
            Meeple {
                meeple_action: Some(meeple_action),
                player,
            },
        )
    }

    impl TryInto<ccs::Action> for Action {
        type Error = TryConvertError;

        fn try_into(self) -> Result<ccs::Action, Self::Error> {
            let coord = self.coord.ok_or(try_convert_error("Missing coord field"))?;
            let meeple_action = self
                .meeple_action
                .ok_or(try_convert_error("Missing meeple_action field"))?;
            let rotation = Rotation::from_i32(self.rotation)
                .ok_or(try_convert_error("Out-of-range rotation"))?;

            Ok(ccs::Action {
                coord: coord.try_into()?,
                rotation: rotation.try_into()?,
                meeple_action: meeple_action.try_into()?,
            })
        }
    }

    impl TryInto<ccs::Coord> for Coord {
        type Error = TryConvertError;

        fn try_into(self) -> Result<ccs::Coord, Self::Error> {
            Ok(ccs::Coord {
                i: i8::try_from(self.i)
                    .map_err(|_| try_convert_error("Out-of-range coordinates"))?,
                j: i8::try_from(self.j)
                    .map_err(|_| try_convert_error("Out-of-range coordinates"))?,
            })
        }
    }

    impl TryInto<ccs::Rotation> for Rotation {
        type Error = TryConvertError;

        fn try_into(self) -> Result<ccs::Rotation, Self::Error> {
            let rotation = u8::try_from(self as i32)
                .map_err(|_| try_convert_error("Out-of-range rotation"))?;
            ccs::Rotation::from_u8(rotation).ok_or(try_convert_error("Out-of-range rotation"))
        }
    }

    impl TryInto<ccs::MeepleAction> for MeepleAction {
        type Error = TryConvertError;

        fn try_into(self) -> Result<ccs::MeepleAction, Self::Error> {
            let meeple_action = self
                .meeple_action
                .ok_or(try_convert_error("Missing meeple_action field"))?;
            Ok(match meeple_action {
                meeple_action::MeepleAction::None(_) => ccs::MeepleAction::None,
                meeple_action::MeepleAction::Edge(number) => ccs::MeepleAction::Edge(
                    u8::try_from(number)
                        .map_err(|_| try_convert_error("Out-of-range edge number"))?,
                ),
                meeple_action::MeepleAction::Cloister(_) => ccs::MeepleAction::Cloister,
            })
        }
    }

    #[derive(Debug)]
    pub struct TryConvertError {
        pub msg: &'static str,
    }

    fn try_convert_error(msg: &'static str) -> TryConvertError {
        TryConvertError { msg }
    }

    impl std::fmt::Display for TryConvertError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{}", self.msg)
        }
    }

    impl std::error::Error for TryConvertError {}
}

fn init_tracing() -> Result<tracing_appender::non_blocking::WorkerGuard> {
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::{fmt, EnvFilter};

    let file_appender = tracing_appender::rolling::hourly("logs", "carcassonne.log");
    let (non_blocking, _guard) = tracing_appender::non_blocking(file_appender);

    let collector = tracing_subscriber::registry()
        .with(tracing_journald::layer()?)
        .with(EnvFilter::from_default_env())
        .with(fmt::Layer::new().with_writer(std::io::stdout))
        .with(fmt::Layer::new().with_writer(non_blocking));
    tracing::subscriber::set_global_default(collector)?;

    Ok(_guard)
}
