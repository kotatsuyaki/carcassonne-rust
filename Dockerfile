FROM pytorch/pytorch:1.9.0-cuda11.1-cudnn8-runtime

WORKDIR /data/
RUN apt-get update && \
    apt-get install -y protobuf-compiler make && \
    pip install torch-scatter torch-sparse torch-cluster torch-spline-conv torch-geometric \
    -f https://data.pyg.org/whl/torch-1.9.0+cu111.html && \
    pip install \
    readerwriterlock \
    numpy \
    grpcio \
    grpcio-tools \
    grpclib \
    aiosqlite \
    aiorwlock \
    mypy-protobuf \
    ipython \
    matplotlib \
    jupyterlab \
    ipywidgets \
    python3-dev \
    curl \
    gcc
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
ENV NVIDIA_VISIBLE_DEVICES=all
ENV LD_LIBRARY_PATH=/opt/conda/pkgs/python-3.7.10-hdb3f193_0/lib/
